package org.horizen.waterconservation.Root;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010-\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010\u00132\u0006\u0010/\u001a\u00020\u000fR \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\u0007\"\u0004\b\r\u0010\tR \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0007\"\u0004\b\u0011\u0010\tR(\u0010\u0012\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150\u0014\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0007\"\u0004\b\u001d\u0010\tR \u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0007\"\u0004\b!\u0010\tR \u0010\"\u001a\b\u0012\u0004\u0012\u00020#0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0007\"\u0004\b%\u0010\tR(\u0010&\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\'0\u0014\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0017\"\u0004\b)\u0010\u0019R(\u0010*\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0014\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0017\"\u0004\b,\u0010\u0019\u00a8\u00060"}, d2 = {"Lorg/horizen/waterconservation/Root/RootVm;", "Landroid/arch/lifecycle/ViewModel;", "()V", "activeUser", "Landroid/arch/lifecycle/MutableLiveData;", "Lorg/horizen/waterconservation/Models/User;", "getActiveUser", "()Landroid/arch/lifecycle/MutableLiveData;", "setActiveUser", "(Landroid/arch/lifecycle/MutableLiveData;)V", "authState", "", "getAuthState", "setAuthState", "authStateMessage", "", "getAuthStateMessage", "setAuthStateMessage", "contacts", "Landroid/arch/lifecycle/LiveData;", "", "Lorg/horizen/waterconservation/Models/ContactData;", "getContacts", "()Landroid/arch/lifecycle/LiveData;", "setContacts", "(Landroid/arch/lifecycle/LiveData;)V", "currentLocaton", "Landroid/location/Location;", "getCurrentLocaton", "setCurrentLocaton", "currentProblemLocation", "Lorg/horizen/waterconservation/Models/ProblemLocation;", "getCurrentProblemLocation", "setCurrentProblemLocation", "internetAvailable", "", "getInternetAvailable", "setInternetAvailable", "problems", "Lorg/horizen/waterconservation/Models/ProblemData;", "getProblems", "setProblems", "users", "getUsers", "setUsers", "getUserLocation", "Lcom/google/android/gms/maps/model/LatLng;", "id", "app_debug"})
public final class RootVm extends android.arch.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private android.arch.lifecycle.MutableLiveData<java.lang.Integer> authState;
    @org.jetbrains.annotations.NotNull()
    private android.arch.lifecycle.MutableLiveData<org.horizen.waterconservation.Models.User> activeUser;
    @org.jetbrains.annotations.NotNull()
    private android.arch.lifecycle.MutableLiveData<java.lang.String> authStateMessage;
    @org.jetbrains.annotations.NotNull()
    private android.arch.lifecycle.MutableLiveData<java.lang.Boolean> internetAvailable;
    @org.jetbrains.annotations.NotNull()
    private android.arch.lifecycle.MutableLiveData<android.location.Location> currentLocaton;
    @org.jetbrains.annotations.Nullable()
    private android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.User>> users;
    @org.jetbrains.annotations.Nullable()
    private android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.ProblemData>> problems;
    @org.jetbrains.annotations.Nullable()
    private android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.ContactData>> contacts;
    @org.jetbrains.annotations.NotNull()
    private android.arch.lifecycle.MutableLiveData<org.horizen.waterconservation.Models.ProblemLocation> currentProblemLocation;
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.MutableLiveData<java.lang.Integer> getAuthState() {
        return null;
    }
    
    public final void setAuthState(@org.jetbrains.annotations.NotNull()
    android.arch.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.MutableLiveData<org.horizen.waterconservation.Models.User> getActiveUser() {
        return null;
    }
    
    public final void setActiveUser(@org.jetbrains.annotations.NotNull()
    android.arch.lifecycle.MutableLiveData<org.horizen.waterconservation.Models.User> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.MutableLiveData<java.lang.String> getAuthStateMessage() {
        return null;
    }
    
    public final void setAuthStateMessage(@org.jetbrains.annotations.NotNull()
    android.arch.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.MutableLiveData<java.lang.Boolean> getInternetAvailable() {
        return null;
    }
    
    public final void setInternetAvailable(@org.jetbrains.annotations.NotNull()
    android.arch.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.MutableLiveData<android.location.Location> getCurrentLocaton() {
        return null;
    }
    
    public final void setCurrentLocaton(@org.jetbrains.annotations.NotNull()
    android.arch.lifecycle.MutableLiveData<android.location.Location> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.User>> getUsers() {
        return null;
    }
    
    public final void setUsers(@org.jetbrains.annotations.Nullable()
    android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.User>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.ProblemData>> getProblems() {
        return null;
    }
    
    public final void setProblems(@org.jetbrains.annotations.Nullable()
    android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.ProblemData>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.ContactData>> getContacts() {
        return null;
    }
    
    public final void setContacts(@org.jetbrains.annotations.Nullable()
    android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.ContactData>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.MutableLiveData<org.horizen.waterconservation.Models.ProblemLocation> getCurrentProblemLocation() {
        return null;
    }
    
    public final void setCurrentProblemLocation(@org.jetbrains.annotations.NotNull()
    android.arch.lifecycle.MutableLiveData<org.horizen.waterconservation.Models.ProblemLocation> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.arch.lifecycle.LiveData<com.google.android.gms.maps.model.LatLng> getUserLocation(@org.jetbrains.annotations.NotNull()
    java.lang.String id) {
        return null;
    }
    
    public RootVm() {
        super();
    }
}