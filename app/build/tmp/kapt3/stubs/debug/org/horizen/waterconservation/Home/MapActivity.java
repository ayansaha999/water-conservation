package org.horizen.waterconservation.Home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u00012B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\"\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001e2\b\u0010 \u001a\u0004\u0018\u00010!H\u0014J\b\u0010\"\u001a\u00020\u0016H\u0016J\u0012\u0010#\u001a\u00020\u00162\b\u0010$\u001a\u0004\u0018\u00010%H\u0014J\u0012\u0010&\u001a\u00020\u00162\b\u0010\'\u001a\u0004\u0018\u00010\fH\u0016J\u0018\u0010(\u001a\u00020\u00162\u000e\u0010)\u001a\n\u0012\u0004\u0012\u00020+\u0018\u00010*H\u0016J\u0012\u0010,\u001a\u00020\u00162\b\u0010-\u001a\u0004\u0018\u00010.H\u0017J\b\u0010/\u001a\u00020\u0016H\u0002J\b\u00100\u001a\u00020\u0016H\u0002J\b\u00101\u001a\u00020\u0016H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\f0\bj\b\u0012\u0004\u0012\u00020\f`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\f0\bj\b\u0012\u0004\u0012\u00020\f`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u00100\bj\b\u0012\u0004\u0012\u00020\u0010`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\f0\bj\b\u0012\u0004\u0012\u00020\f`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00063"}, d2 = {"Lorg/horizen/waterconservation/Home/MapActivity;", "Lorg/horizen/waterconservation/Root/RootActivity;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "Lin/net/freak/androidcore/Picker/api/callbacks/ImagePickerCallback;", "()V", "cameraPicker", "Lin/net/freak/androidcore/Picker/api/CameraImagePicker;", "contactList", "Ljava/util/ArrayList;", "Lorg/horizen/waterconservation/Models/ContactData;", "Lkotlin/collections/ArrayList;", "pickerPath", "", "pipeTypeList", "problemIdList", "problemList", "Lorg/horizen/waterconservation/Models/ProblemData;", "problemNameList", "selectedContactList", "sheetBehavior", "Landroid/support/design/widget/BottomSheetBehavior;", "capture", "", "getCustomLayersFromRaw", "getOutputMediaFileUri", "Ljava/io/File;", "initializeForm", "loadLayers", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onError", "error", "onImagesChosen", "chosenImages", "", "Lin/net/freak/androidcore/Picker/api/entity/ChosenImage;", "onMapReady", "map", "Lcom/google/android/gms/maps/GoogleMap;", "toggleProblemSheet", "zoomIn", "zoomOut", "ContactListAdapter", "app_debug"})
public final class MapActivity extends org.horizen.waterconservation.Root.RootActivity implements com.google.android.gms.maps.OnMapReadyCallback, in.net.freak.androidcore.Picker.api.callbacks.ImagePickerCallback {
    private android.support.design.widget.BottomSheetBehavior<?> sheetBehavior;
    private java.util.ArrayList<java.lang.String> pipeTypeList;
    private java.util.ArrayList<org.horizen.waterconservation.Models.ProblemData> problemList;
    private java.util.ArrayList<org.horizen.waterconservation.Models.ContactData> contactList;
    private java.util.ArrayList<org.horizen.waterconservation.Models.ContactData> selectedContactList;
    private java.util.ArrayList<java.lang.String> problemIdList;
    private java.util.ArrayList<java.lang.String> problemNameList;
    private in.net.freak.androidcore.Picker.api.CameraImagePicker cameraPicker;
    private java.lang.String pickerPath;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void toggleProblemSheet() {
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap map) {
    }
    
    private final void getCustomLayersFromRaw() {
    }
    
    private final void loadLayers() {
    }
    
    private final void zoomIn() {
    }
    
    private final void zoomOut() {
    }
    
    private final void initializeForm() {
    }
    
    @java.lang.Override()
    public void onImagesChosen(@org.jetbrains.annotations.Nullable()
    java.util.List<in.net.freak.androidcore.Picker.api.entity.ChosenImage> chosenImages) {
    }
    
    @java.lang.Override()
    public void onError(@org.jetbrains.annotations.Nullable()
    java.lang.String error) {
    }
    
    private final void capture() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final java.io.File getOutputMediaFileUri() {
        return null;
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public MapActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\nH\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\nH\u0016J$\u0010\u0010\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\n2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016R\u001e\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lorg/horizen/waterconservation/Home/MapActivity$ContactListAdapter;", "Landroid/widget/BaseAdapter;", "contacts", "Ljava/util/ArrayList;", "Lorg/horizen/waterconservation/Models/ContactData;", "Lkotlin/collections/ArrayList;", "(Lorg/horizen/waterconservation/Home/MapActivity;Ljava/util/ArrayList;)V", "mInflater", "Landroid/view/LayoutInflater;", "getCount", "", "getItem", "", "position", "getItemId", "", "getView", "Landroid/view/View;", "convertView", "parent", "Landroid/view/ViewGroup;", "app_debug"})
    public final class ContactListAdapter extends android.widget.BaseAdapter {
        private final android.view.LayoutInflater mInflater = null;
        private final java.util.ArrayList<org.horizen.waterconservation.Models.ContactData> contacts = null;
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public android.view.View getView(int position, @org.jetbrains.annotations.Nullable()
        android.view.View convertView, @org.jetbrains.annotations.Nullable()
        android.view.ViewGroup parent) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.Object getItem(int position) {
            return null;
        }
        
        @java.lang.Override()
        public long getItemId(int position) {
            return 0L;
        }
        
        @java.lang.Override()
        public int getCount() {
            return 0;
        }
        
        public ContactListAdapter(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<org.horizen.waterconservation.Models.ContactData> contacts) {
            super();
        }
    }
}