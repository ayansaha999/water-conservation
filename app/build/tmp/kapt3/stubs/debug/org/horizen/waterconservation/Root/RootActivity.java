package org.horizen.waterconservation.Root;

import java.lang.System;

@android.annotation.SuppressLint(value = {"Registered"})
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0017\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010O\u001a\u00020PH\u0002J\u0006\u0010Q\u001a\u00020PJ\b\u0010R\u001a\u00020PH\u0002J\b\u0010S\u001a\u00020PH\u0007J\u001c\u0010T\u001a\u00020.2\u0006\u0010U\u001a\u00020.2\f\u0010V\u001a\b\u0012\u0004\u0012\u00020X0WJ\"\u0010Y\u001a\u00020P2\u0006\u0010Z\u001a\u00020[2\u0006\u0010\\\u001a\u00020[2\b\u0010]\u001a\u0004\u0018\u00010^H\u0014J\u0012\u0010_\u001a\u00020P2\b\u0010`\u001a\u0004\u0018\u00010aH\u0014J\u0010\u0010b\u001a\u00020P2\u0006\u0010c\u001a\u000209H\u0016J\b\u0010d\u001a\u00020PH\u0014J\b\u0010e\u001a\u00020PH\u0002R\u001a\u0010\u0004\u001a\u00020\u0005X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\u00020\u000bX\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u00020\u0011X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001a\u0010\u001c\u001a\u00020\u001dX\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0014\u0010\"\u001a\u00020#X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u001c\u0010&\u001a\u0004\u0018\u00010\'X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\b\u0012\u0004\u0012\u00020.0-X\u0084\u0004\u00a2\u0006\n\n\u0002\u00101\u001a\u0004\b/\u00100R\u001a\u00102\u001a\u000203X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001a\u00108\u001a\u000209X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u00020?X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010CR\u0014\u0010D\u001a\u00020.X\u0084D\u00a2\u0006\b\n\u0000\u001a\u0004\bE\u0010FR\u0014\u0010G\u001a\u00020.X\u0084D\u00a2\u0006\b\n\u0000\u001a\u0004\bH\u0010FR\u001c\u0010I\u001a\u0004\u0018\u00010JX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bK\u0010L\"\u0004\bM\u0010N\u00a8\u0006f"}, d2 = {"Lorg/horizen/waterconservation/Root/RootActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Lorg/horizen/waterconservation/Helper/ConnectivityReceiver$ConnectivityReceiverListener;", "()V", "builder", "Lcom/google/android/gms/location/LocationSettingsRequest$Builder;", "getBuilder", "()Lcom/google/android/gms/location/LocationSettingsRequest$Builder;", "setBuilder", "(Lcom/google/android/gms/location/LocationSettingsRequest$Builder;)V", "core", "Lin/net/freak/androidcore/AndroidCore;", "getCore", "()Lin/net/freak/androidcore/AndroidCore;", "setCore", "(Lin/net/freak/androidcore/AndroidCore;)V", "fusedLocationClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "getFusedLocationClient", "()Lcom/google/android/gms/location/FusedLocationProviderClient;", "setFusedLocationClient", "(Lcom/google/android/gms/location/FusedLocationProviderClient;)V", "localDb", "Lorg/horizen/waterconservation/Helper/LocalDatabase;", "getLocalDb", "()Lorg/horizen/waterconservation/Helper/LocalDatabase;", "setLocalDb", "(Lorg/horizen/waterconservation/Helper/LocalDatabase;)V", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "getLocationCallback", "()Lcom/google/android/gms/location/LocationCallback;", "setLocationCallback", "(Lcom/google/android/gms/location/LocationCallback;)V", "locationRequest", "Lcom/google/android/gms/location/LocationRequest;", "getLocationRequest", "()Lcom/google/android/gms/location/LocationRequest;", "mMap", "Lcom/google/android/gms/maps/GoogleMap;", "getMMap", "()Lcom/google/android/gms/maps/GoogleMap;", "setMMap", "(Lcom/google/android/gms/maps/GoogleMap;)V", "permissions", "", "", "getPermissions", "()[Ljava/lang/String;", "[Ljava/lang/String;", "prefs", "Lin/net/freak/androidcore/Pref;", "getPrefs", "()Lin/net/freak/androidcore/Pref;", "setPrefs", "(Lin/net/freak/androidcore/Pref;)V", "requestingLocationUpdates", "", "getRequestingLocationUpdates", "()Z", "setRequestingLocationUpdates", "(Z)V", "rootVm", "Lorg/horizen/waterconservation/Root/RootVm;", "getRootVm", "()Lorg/horizen/waterconservation/Root/RootVm;", "setRootVm", "(Lorg/horizen/waterconservation/Root/RootVm;)V", "serverFileUrl", "getServerFileUrl", "()Ljava/lang/String;", "serverUrl", "getServerUrl", "user", "Lorg/horizen/waterconservation/Models/User;", "getUser", "()Lorg/horizen/waterconservation/Models/User;", "setUser", "(Lorg/horizen/waterconservation/Models/User;)V", "downloadContactsData", "", "downloadLocationData", "downloadProblemData", "getLastLocation", "getRequestString", "api", "valuePairs", "Ljava/util/ArrayList;", "Lin/net/freak/androidcore/ValuePair;", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onNetworkConnectionChanged", "isConnected", "onResume", "startLocationUpdates", "app_debug"})
public class RootActivity extends android.support.v7.app.AppCompatActivity implements org.horizen.waterconservation.Helper.ConnectivityReceiver.ConnectivityReceiverListener {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String[] permissions = null;
    @org.jetbrains.annotations.NotNull()
    protected org.horizen.waterconservation.Root.RootVm rootVm;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String serverUrl = "http://maps.wbphed.gov.in/water_conservation/webservice/";
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String serverFileUrl = "http://maps.wbphed.gov.in/water_conservation/webservice/data/kml/";
    @org.jetbrains.annotations.NotNull()
    protected in.net.freak.androidcore.AndroidCore core;
    @org.jetbrains.annotations.NotNull()
    protected in.net.freak.androidcore.Pref prefs;
    @org.jetbrains.annotations.Nullable()
    private org.horizen.waterconservation.Helper.LocalDatabase localDb;
    @org.jetbrains.annotations.Nullable()
    private org.horizen.waterconservation.Models.User user;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.maps.GoogleMap mMap;
    @org.jetbrains.annotations.NotNull()
    private com.google.android.gms.location.LocationSettingsRequest.Builder builder;
    @org.jetbrains.annotations.NotNull()
    protected com.google.android.gms.location.FusedLocationProviderClient fusedLocationClient;
    @org.jetbrains.annotations.NotNull()
    protected com.google.android.gms.location.LocationCallback locationCallback;
    @org.jetbrains.annotations.NotNull()
    private final com.google.android.gms.location.LocationRequest locationRequest = null;
    private boolean requestingLocationUpdates;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    protected final java.lang.String[] getPermissions() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final org.horizen.waterconservation.Root.RootVm getRootVm() {
        return null;
    }
    
    protected final void setRootVm(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Root.RootVm p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final java.lang.String getServerUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final java.lang.String getServerFileUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final in.net.freak.androidcore.AndroidCore getCore() {
        return null;
    }
    
    protected final void setCore(@org.jetbrains.annotations.NotNull()
    in.net.freak.androidcore.AndroidCore p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final in.net.freak.androidcore.Pref getPrefs() {
        return null;
    }
    
    protected final void setPrefs(@org.jetbrains.annotations.NotNull()
    in.net.freak.androidcore.Pref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final org.horizen.waterconservation.Helper.LocalDatabase getLocalDb() {
        return null;
    }
    
    protected final void setLocalDb(@org.jetbrains.annotations.Nullable()
    org.horizen.waterconservation.Helper.LocalDatabase p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final org.horizen.waterconservation.Models.User getUser() {
        return null;
    }
    
    protected final void setUser(@org.jetbrains.annotations.Nullable()
    org.horizen.waterconservation.Models.User p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.google.android.gms.maps.GoogleMap getMMap() {
        return null;
    }
    
    protected final void setMMap(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.google.android.gms.location.LocationSettingsRequest.Builder getBuilder() {
        return null;
    }
    
    protected final void setBuilder(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.location.LocationSettingsRequest.Builder p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.google.android.gms.location.FusedLocationProviderClient getFusedLocationClient() {
        return null;
    }
    
    protected final void setFusedLocationClient(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.location.FusedLocationProviderClient p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.google.android.gms.location.LocationCallback getLocationCallback() {
        return null;
    }
    
    protected final void setLocationCallback(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.location.LocationCallback p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.google.android.gms.location.LocationRequest getLocationRequest() {
        return null;
    }
    
    protected final boolean getRequestingLocationUpdates() {
        return false;
    }
    
    protected final void setRequestingLocationUpdates(boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onNetworkConnectionChanged(boolean isConnected) {
    }
    
    public final void downloadLocationData() {
    }
    
    private final void downloadProblemData() {
    }
    
    private final void downloadContactsData() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRequestString(@org.jetbrains.annotations.NotNull()
    java.lang.String api, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<in.net.freak.androidcore.ValuePair> valuePairs) {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    public final void getLastLocation() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void startLocationUpdates() {
    }
    
    public RootActivity() {
        super();
    }
}