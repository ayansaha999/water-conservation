package org.horizen.waterconservation.Helper;

import java.lang.System;

@android.arch.persistence.room.TypeConverters(value = {org.horizen.waterconservation.Helper.DataTypeConverter.class})
@android.arch.persistence.room.Database(entities = {org.horizen.waterconservation.Models.User.class, org.horizen.waterconservation.Models.LocationData.class, org.horizen.waterconservation.Models.ProblemData.class, org.horizen.waterconservation.Models.ContactData.class, org.horizen.waterconservation.Models.ProblemLocation.class, org.horizen.waterconservation.Models.CustomMapLayer.class}, version = 2)
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\bH&J\b\u0010\t\u001a\u00020\nH&J\b\u0010\u000b\u001a\u00020\fH&J\b\u0010\r\u001a\u00020\u000eH&\u00a8\u0006\u000f"}, d2 = {"Lorg/horizen/waterconservation/Helper/LocalDatabase;", "Landroid/arch/persistence/room/RoomDatabase;", "()V", "contactDataDao", "Lorg/horizen/waterconservation/Daos/ContactDao;", "customMapLayerDao", "Lorg/horizen/waterconservation/Daos/CustomMapLayerDao;", "locationDataDao", "Lorg/horizen/waterconservation/Daos/LocationDao;", "problemDataDao", "Lorg/horizen/waterconservation/Daos/ProblemDao;", "problemLocationDao", "Lorg/horizen/waterconservation/Daos/ProblemLocationDao;", "userDao", "Lorg/horizen/waterconservation/Daos/UserDao;", "app_debug"})
public abstract class LocalDatabase extends android.arch.persistence.room.RoomDatabase {
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.horizen.waterconservation.Daos.UserDao userDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.horizen.waterconservation.Daos.LocationDao locationDataDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.horizen.waterconservation.Daos.ProblemDao problemDataDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.horizen.waterconservation.Daos.ContactDao contactDataDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.horizen.waterconservation.Daos.ProblemLocationDao problemLocationDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.horizen.waterconservation.Daos.CustomMapLayerDao customMapLayerDao();
    
    public LocalDatabase() {
        super();
    }
}