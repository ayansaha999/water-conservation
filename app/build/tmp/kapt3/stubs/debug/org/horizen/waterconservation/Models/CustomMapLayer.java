package org.horizen.waterconservation.Models;

import java.lang.System;

@android.arch.persistence.room.Entity()
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0013\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B/\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0018\u001a\u00020\u0004H\u00c6\u0003J\u0019\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u00c6\u0003J\t\u0010\u001a\u001a\u00020\nH\u00c6\u0003J7\u0010\u001b\u001a\u00020\u00002\b\b\u0003\u0010\u0003\u001a\u00020\u00042\u0018\b\u0002\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b2\b\b\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\n2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\u0004H\u00d6\u0001R*\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017\u00a8\u0006\""}, d2 = {"Lorg/horizen/waterconservation/Models/CustomMapLayer;", "Ljava/io/Serializable;", "()V", "key", "", "feature", "Ljava/util/ArrayList;", "Lin/net/freak/androidcore/Map/GeoJson/MapLayerFeature;", "Lkotlin/collections/ArrayList;", "visible", "", "(Ljava/lang/String;Ljava/util/ArrayList;Z)V", "getFeature", "()Ljava/util/ArrayList;", "setFeature", "(Ljava/util/ArrayList;)V", "getKey", "()Ljava/lang/String;", "setKey", "(Ljava/lang/String;)V", "getVisible", "()Z", "setVisible", "(Z)V", "component1", "component2", "component3", "copy", "equals", "other", "", "hashCode", "", "toString", "app_debug"})
public final class CustomMapLayer implements java.io.Serializable {
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.PrimaryKey()
    private java.lang.String key;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> feature;
    private boolean visible;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getKey() {
        return null;
    }
    
    public final void setKey(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> getFeature() {
        return null;
    }
    
    public final void setFeature(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> p0) {
    }
    
    public final boolean getVisible() {
        return false;
    }
    
    public final void setVisible(boolean p0) {
    }
    
    public CustomMapLayer(@org.jetbrains.annotations.NotNull()
    @android.support.annotation.NonNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> feature, boolean visible) {
        super();
    }
    
    public CustomMapLayer() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> component2() {
        return null;
    }
    
    public final boolean component3() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.horizen.waterconservation.Models.CustomMapLayer copy(@org.jetbrains.annotations.NotNull()
    @android.support.annotation.NonNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> feature, boolean visible) {
        return null;
    }
    
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(java.lang.Object p0) {
        return false;
    }
}