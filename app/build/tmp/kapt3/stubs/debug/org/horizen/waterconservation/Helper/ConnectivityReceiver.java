package org.horizen.waterconservation.Helper;

import java.lang.System;

/**
 * * Created by Ayan Saha from HORIZEN on 6/16/2017.
 */
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u0000 \t2\u00020\u0001:\u0002\t\nB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0017\u00a8\u0006\u000b"}, d2 = {"Lorg/horizen/waterconservation/Helper/ConnectivityReceiver;", "Landroid/content/BroadcastReceiver;", "()V", "onReceive", "", "context", "Landroid/content/Context;", "arg1", "Landroid/content/Intent;", "Companion", "ConnectivityReceiverListener", "app_debug"})
public class ConnectivityReceiver extends android.content.BroadcastReceiver {
    @org.jetbrains.annotations.Nullable()
    private static org.horizen.waterconservation.Helper.ConnectivityReceiver.ConnectivityReceiverListener connectivityReceiverListener;
    public static final org.horizen.waterconservation.Helper.ConnectivityReceiver.Companion Companion = null;
    
    @android.annotation.SuppressLint(value = {"UnsafeProtectedBroadcastReceiver"})
    @java.lang.Override()
    public void onReceive(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.content.Intent arg1) {
    }
    
    public ConnectivityReceiver() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lorg/horizen/waterconservation/Helper/ConnectivityReceiver$ConnectivityReceiverListener;", "", "onNetworkConnectionChanged", "", "isConnected", "", "app_debug"})
    public static abstract interface ConnectivityReceiverListener {
        
        public abstract void onNetworkConnectionChanged(boolean isConnected);
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n8F\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lorg/horizen/waterconservation/Helper/ConnectivityReceiver$Companion;", "", "()V", "connectivityReceiverListener", "Lorg/horizen/waterconservation/Helper/ConnectivityReceiver$ConnectivityReceiverListener;", "getConnectivityReceiverListener", "()Lorg/horizen/waterconservation/Helper/ConnectivityReceiver$ConnectivityReceiverListener;", "setConnectivityReceiverListener", "(Lorg/horizen/waterconservation/Helper/ConnectivityReceiver$ConnectivityReceiverListener;)V", "isConnected", "", "()Z", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final org.horizen.waterconservation.Helper.ConnectivityReceiver.ConnectivityReceiverListener getConnectivityReceiverListener() {
            return null;
        }
        
        public final void setConnectivityReceiverListener(@org.jetbrains.annotations.Nullable()
        org.horizen.waterconservation.Helper.ConnectivityReceiver.ConnectivityReceiverListener p0) {
        }
        
        public final boolean isConnected() {
            return false;
        }
        
        private Companion() {
            super();
        }
    }
}