package org.horizen.waterconservation.Helper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0015\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lorg/horizen/waterconservation/Helper/Keys;", "", "()V", "ACTION", "", "BLOCK_BOUNDARY", "CLEAR_WATER_MAIN", "DISTRIBUTION_MAIN", "DOWNLOADED_DATA", "", "DOWNLOADING_DATA", "LOADING_MAP", "LOGGED", "LOGGED_IN", "LOGGED_OUT", "LOGGING_IN", "MAP_DATA_SYNCED", "MOUZA_BOUNDARY", "OHR", "PANCHAYAT_BOUNDARY", "PROGRESS", "RAW_WATER_MAIN", "REQUEST_CHECK_PERMISSION", "REQUEST_CHECK_SETTINGS", "SCHEME_BOUNDARY", "STAND_POST", "TITLE", "USER_ID", "VALVE", "ZONE_BOUNDARY", "app_debug"})
public final class Keys {
    public static final int LOGGING_IN = 0;
    public static final int LOGGED_IN = 1;
    public static final int DOWNLOADING_DATA = 2;
    public static final int DOWNLOADED_DATA = 3;
    public static final int LOADING_MAP = 4;
    public static final int LOGGED_OUT = 5;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_ID = "USER_ID";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOGGED = "LOGGED";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MAP_DATA_SYNCED = "MAP_DATA_SYNCED";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TITLE = "TITLE";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROGRESS = "PROGRESS";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ACTION = "ACTION";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SCHEME_BOUNDARY = "SCHEME BOUNDARY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ZONE_BOUNDARY = "ZONE BOUNDARY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BLOCK_BOUNDARY = "BLOCK BOUNDARY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PANCHAYAT_BOUNDARY = "PANCHAYAT BOUNDARY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MOUZA_BOUNDARY = "MOUZA BOUNDARY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RAW_WATER_MAIN = "RAW WATER MAIN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CLEAR_WATER_MAIN = "CLEAR WATER MAIN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DISTRIBUTION_MAIN = "DISTRIBUTION MAIN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String STAND_POST = "STAND POST";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALVE = "VALVE";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String OHR = "OVER HEAD RESERVOIR";
    public static final int REQUEST_CHECK_SETTINGS = 1001;
    public static final int REQUEST_CHECK_PERMISSION = 1002;
    public static final org.horizen.waterconservation.Helper.Keys INSTANCE = null;
    
    private Keys() {
        super();
    }
}