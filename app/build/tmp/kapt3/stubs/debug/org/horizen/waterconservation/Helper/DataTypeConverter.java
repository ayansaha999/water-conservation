package org.horizen.waterconservation.Helper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u0007J \u0010\t\u001a\u00020\u00042\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0006j\b\u0012\u0004\u0012\u00020\n`\bH\u0007J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\fH\u0007J\u0010\u0010\r\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u000eH\u0007J\u0010\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0010H\u0007J\u001e\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0006j\b\u0012\u0004\u0012\u00020\n`\b2\u0006\u0010\u0005\u001a\u00020\u0001J\u000e\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001J\u0010\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0014H\u0007J \u0010\u0015\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0006j\b\u0012\u0004\u0012\u00020\n`\b2\u0006\u0010\u0016\u001a\u00020\u0004H\u0002J \u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b2\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J \u0010\u0018\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0006j\b\u0012\u0004\u0012\u00020\n`\b2\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J\u0010\u0010\u0019\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J\u0010\u0010\u001a\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J\u0010\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J\u0010\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u001eH\u0007\u00a8\u0006 "}, d2 = {"Lorg/horizen/waterconservation/Helper/DataTypeConverter;", "", "()V", "fromArrayMapFeatureToString", "", "data", "Ljava/util/ArrayList;", "Lin/net/freak/androidcore/Map/GeoJson/MapLayerFeature;", "Lkotlin/collections/ArrayList;", "fromArrayValuePairToString", "Lin/net/freak/androidcore/ValuePair;", "fromContactDataToString", "Lorg/horizen/waterconservation/Models/ContactData;", "fromLatLngToString", "Lcom/google/android/gms/maps/model/LatLng;", "fromLocationToString", "Lorg/horizen/waterconservation/Models/LocationData;", "fromObjectToArrayList", "fromObjectToJsonString", "fromProblemDataToString", "Lorg/horizen/waterconservation/Models/ProblemData;", "fromStringToArrayList", "dataString", "fromStringToArrayMapFeature", "fromStringToArrayValuePair", "fromStringToContactData", "fromStringToLatLng", "fromStringToLocation", "fromStringToProblemData", "fromStringToUser", "Lorg/horizen/waterconservation/Models/User;", "fromUserToString", "app_debug"})
public final class DataTypeConverter {
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.lang.String fromArrayMapFeatureToString(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.util.ArrayList<in.net.freak.androidcore.Map.GeoJson.MapLayerFeature> fromStringToArrayMapFeature(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.lang.String fromArrayValuePairToString(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<in.net.freak.androidcore.ValuePair> data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.util.ArrayList<in.net.freak.androidcore.ValuePair> fromStringToArrayValuePair(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.lang.String fromUserToString(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.User data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final org.horizen.waterconservation.Models.User fromStringToUser(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.lang.String fromProblemDataToString(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.ProblemData data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final org.horizen.waterconservation.Models.ProblemData fromStringToProblemData(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.lang.String fromContactDataToString(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.ContactData data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final org.horizen.waterconservation.Models.ContactData fromStringToContactData(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.lang.String fromLocationToString(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.LocationData data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final org.horizen.waterconservation.Models.LocationData fromStringToLocation(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final java.lang.String fromLatLngToString(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.model.LatLng data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.TypeConverter()
    public final com.google.android.gms.maps.model.LatLng fromStringToLatLng(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String fromObjectToJsonString(@org.jetbrains.annotations.NotNull()
    java.lang.Object data) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<in.net.freak.androidcore.ValuePair> fromObjectToArrayList(@org.jetbrains.annotations.NotNull()
    java.lang.Object data) {
        return null;
    }
    
    private final java.util.ArrayList<in.net.freak.androidcore.ValuePair> fromStringToArrayList(java.lang.String dataString) {
        return null;
    }
    
    public DataTypeConverter() {
        super();
    }
}