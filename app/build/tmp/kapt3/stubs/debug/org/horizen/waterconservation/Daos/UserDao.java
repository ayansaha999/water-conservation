package org.horizen.waterconservation.Daos;

import java.lang.System;

@android.arch.persistence.room.Dao()
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH\'J\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u0007\u001a\u00020\bH\'J\u0014\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\r0\nH\'J!\u0010\u000e\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u000f\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\u0010J!\u0010\u0011\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u000f\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0012"}, d2 = {"Lorg/horizen/waterconservation/Daos/UserDao;", "", "deleteUser", "", "User", "Lorg/horizen/waterconservation/Models/User;", "findUserById", "id", "", "findUserLocation", "Landroid/arch/lifecycle/LiveData;", "Lcom/google/android/gms/maps/model/LatLng;", "getAllUsers", "", "insertUser", "", "([Lorg/horizen/waterconservation/Models/User;)V", "updateUser", "app_debug"})
public abstract interface UserDao {
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "select * from User order by user_name")
    public abstract android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.User>> getAllUsers();
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "select * from User where user_id = :id")
    public abstract org.horizen.waterconservation.Models.User findUserById(@org.jetbrains.annotations.NotNull()
    java.lang.String id);
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "select location from User where user_id = :id")
    public abstract android.arch.lifecycle.LiveData<com.google.android.gms.maps.model.LatLng> findUserLocation(@org.jetbrains.annotations.NotNull()
    java.lang.String id);
    
    @android.arch.persistence.room.Insert(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void insertUser(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.User... User);
    
    @android.arch.persistence.room.Update(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void updateUser(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.User... User);
    
    @android.arch.persistence.room.Delete()
    public abstract void deleteUser(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.User User);
}