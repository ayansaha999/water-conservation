package org.horizen.waterconservation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \b2\u00020\u0001:\u0001\bB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\t"}, d2 = {"Lorg/horizen/waterconservation/RootApplication;", "Landroid/app/Application;", "()V", "onCreate", "", "setConnectivityListener", "listener", "Lorg/horizen/waterconservation/Helper/ConnectivityReceiver$ConnectivityReceiverListener;", "Companion", "app_debug"})
public final class RootApplication extends android.app.Application {
    @org.jetbrains.annotations.Nullable()
    private static org.horizen.waterconservation.RootApplication instance;
    @org.jetbrains.annotations.Nullable()
    private static org.horizen.waterconservation.Helper.LocalDatabase database;
    public static final org.horizen.waterconservation.RootApplication.Companion Companion = null;
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    public final void setConnectivityListener(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Helper.ConnectivityReceiver.ConnectivityReceiverListener listener) {
    }
    
    public RootApplication() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lorg/horizen/waterconservation/RootApplication$Companion;", "", "()V", "database", "Lorg/horizen/waterconservation/Helper/LocalDatabase;", "getDatabase", "()Lorg/horizen/waterconservation/Helper/LocalDatabase;", "setDatabase", "(Lorg/horizen/waterconservation/Helper/LocalDatabase;)V", "instance", "Lorg/horizen/waterconservation/RootApplication;", "getInstance", "()Lorg/horizen/waterconservation/RootApplication;", "setInstance", "(Lorg/horizen/waterconservation/RootApplication;)V", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final org.horizen.waterconservation.RootApplication getInstance() {
            return null;
        }
        
        public final void setInstance(@org.jetbrains.annotations.Nullable()
        org.horizen.waterconservation.RootApplication p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final org.horizen.waterconservation.Helper.LocalDatabase getDatabase() {
            return null;
        }
        
        public final void setDatabase(@org.jetbrains.annotations.Nullable()
        org.horizen.waterconservation.Helper.LocalDatabase p0) {
        }
        
        private Companion() {
            super();
        }
    }
}