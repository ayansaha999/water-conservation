package org.horizen.waterconservation.Daos;

import java.lang.System;

@android.arch.persistence.room.Dao()
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH\'J\u0014\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u000b0\nH\'J!\u0010\f\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\r\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\u000eJ!\u0010\u000f\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\r\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u0010"}, d2 = {"Lorg/horizen/waterconservation/Daos/LocationDao;", "", "deleteLocationData", "", "LocationData", "Lorg/horizen/waterconservation/Models/LocationData;", "findLocationDataById", "id", "", "getAllLocationDatas", "Landroid/arch/lifecycle/LiveData;", "", "insertLocationData", "", "([Lorg/horizen/waterconservation/Models/LocationData;)V", "updateLocationData", "app_debug"})
public abstract interface LocationDao {
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "select * from LocationData order by project_name")
    public abstract android.arch.lifecycle.LiveData<java.util.List<org.horizen.waterconservation.Models.LocationData>> getAllLocationDatas();
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "select * from LocationData where project_id = :id")
    public abstract org.horizen.waterconservation.Models.LocationData findLocationDataById(@org.jetbrains.annotations.NotNull()
    java.lang.String id);
    
    @android.arch.persistence.room.Insert(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void insertLocationData(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.LocationData... LocationData);
    
    @android.arch.persistence.room.Update(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void updateLocationData(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.LocationData... LocationData);
    
    @android.arch.persistence.room.Delete()
    public abstract void deleteLocationData(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.LocationData LocationData);
}