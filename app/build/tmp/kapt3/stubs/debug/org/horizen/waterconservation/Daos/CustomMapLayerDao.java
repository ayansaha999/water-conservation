package org.horizen.waterconservation.Daos;

import java.lang.System;

@android.arch.persistence.room.Dao()
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H\'J!\u0010\b\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\t\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\nJ!\u0010\u000b\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\t\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\n\u00a8\u0006\f"}, d2 = {"Lorg/horizen/waterconservation/Daos/CustomMapLayerDao;", "", "deleteCustomMapLayer", "", "CustomMapLayer", "Lorg/horizen/waterconservation/Models/CustomMapLayer;", "getAllCustomMapLayers", "", "insertCustomMapLayer", "", "([Lorg/horizen/waterconservation/Models/CustomMapLayer;)V", "updateCustomMapLayer", "app_debug"})
public abstract interface CustomMapLayerDao {
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "select * from CustomMapLayer")
    public abstract java.util.List<org.horizen.waterconservation.Models.CustomMapLayer> getAllCustomMapLayers();
    
    @android.arch.persistence.room.Insert(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void insertCustomMapLayer(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.CustomMapLayer... CustomMapLayer);
    
    @android.arch.persistence.room.Update(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void updateCustomMapLayer(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.CustomMapLayer... CustomMapLayer);
    
    @android.arch.persistence.room.Delete()
    public abstract void deleteCustomMapLayer(@org.jetbrains.annotations.NotNull()
    org.horizen.waterconservation.Models.CustomMapLayer CustomMapLayer);
}