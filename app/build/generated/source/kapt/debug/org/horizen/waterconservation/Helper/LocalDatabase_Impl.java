package org.horizen.waterconservation.Helper;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import org.horizen.waterconservation.Daos.ContactDao;
import org.horizen.waterconservation.Daos.ContactDao_Impl;
import org.horizen.waterconservation.Daos.CustomMapLayerDao;
import org.horizen.waterconservation.Daos.CustomMapLayerDao_Impl;
import org.horizen.waterconservation.Daos.LocationDao;
import org.horizen.waterconservation.Daos.LocationDao_Impl;
import org.horizen.waterconservation.Daos.ProblemDao;
import org.horizen.waterconservation.Daos.ProblemDao_Impl;
import org.horizen.waterconservation.Daos.ProblemLocationDao;
import org.horizen.waterconservation.Daos.ProblemLocationDao_Impl;
import org.horizen.waterconservation.Daos.UserDao;
import org.horizen.waterconservation.Daos.UserDao_Impl;

@SuppressWarnings("unchecked")
public class LocalDatabase_Impl extends LocalDatabase {
  private volatile UserDao _userDao;

  private volatile LocationDao _locationDao;

  private volatile ProblemDao _problemDao;

  private volatile ContactDao _contactDao;

  private volatile ProblemLocationDao _problemLocationDao;

  private volatile CustomMapLayerDao _customMapLayerDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(2) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `User` (`user_id` TEXT NOT NULL, `user_name` TEXT NOT NULL, `password` TEXT NOT NULL, `file_path` TEXT NOT NULL, `version` TEXT NOT NULL, `loggedIn` INTEGER NOT NULL, `location` TEXT NOT NULL, PRIMARY KEY(`user_id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `LocationData` (`id` TEXT NOT NULL, `user_id` TEXT NOT NULL, `district_id` TEXT NOT NULL, `district_name` TEXT NOT NULL, `block_id` TEXT NOT NULL, `block_name` TEXT NOT NULL, `gp_id` TEXT NOT NULL, `gp_name` TEXT NOT NULL, `project_id` TEXT NOT NULL, `project_name` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `ProblemData` (`id` TEXT NOT NULL, `problem` TEXT NOT NULL, `prioroty` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `ContactData` (`id` TEXT NOT NULL, `receiver_name` TEXT NOT NULL, `receiver_designation` TEXT NOT NULL, `receiver_phno` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `ProblemLocation` (`id` TEXT NOT NULL, `user_id` TEXT NOT NULL, `pipiline_type` TEXT NOT NULL, `gp_id` TEXT NOT NULL, `zone_no` TEXT NOT NULL, `mouza_name` TEXT NOT NULL, `problem_id` TEXT NOT NULL, `problem_lat` TEXT NOT NULL, `problem_lon` TEXT NOT NULL, `filePath` TEXT NOT NULL, `resolved` INTEGER NOT NULL, `remarks` TEXT NOT NULL, `uploaded` INTEGER NOT NULL, `date` TEXT NOT NULL, `time` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `CustomMapLayer` (`key` TEXT NOT NULL, `feature` TEXT NOT NULL, `visible` INTEGER NOT NULL, PRIMARY KEY(`key`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"56571c9c89c948e5250c8a00b78d2ccd\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `User`");
        _db.execSQL("DROP TABLE IF EXISTS `LocationData`");
        _db.execSQL("DROP TABLE IF EXISTS `ProblemData`");
        _db.execSQL("DROP TABLE IF EXISTS `ContactData`");
        _db.execSQL("DROP TABLE IF EXISTS `ProblemLocation`");
        _db.execSQL("DROP TABLE IF EXISTS `CustomMapLayer`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsUser = new HashMap<String, TableInfo.Column>(7);
        _columnsUser.put("user_id", new TableInfo.Column("user_id", "TEXT", true, 1));
        _columnsUser.put("user_name", new TableInfo.Column("user_name", "TEXT", true, 0));
        _columnsUser.put("password", new TableInfo.Column("password", "TEXT", true, 0));
        _columnsUser.put("file_path", new TableInfo.Column("file_path", "TEXT", true, 0));
        _columnsUser.put("version", new TableInfo.Column("version", "TEXT", true, 0));
        _columnsUser.put("loggedIn", new TableInfo.Column("loggedIn", "INTEGER", true, 0));
        _columnsUser.put("location", new TableInfo.Column("location", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysUser = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesUser = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoUser = new TableInfo("User", _columnsUser, _foreignKeysUser, _indicesUser);
        final TableInfo _existingUser = TableInfo.read(_db, "User");
        if (! _infoUser.equals(_existingUser)) {
          throw new IllegalStateException("Migration didn't properly handle User(org.horizen.waterconservation.Models.User).\n"
                  + " Expected:\n" + _infoUser + "\n"
                  + " Found:\n" + _existingUser);
        }
        final HashMap<String, TableInfo.Column> _columnsLocationData = new HashMap<String, TableInfo.Column>(10);
        _columnsLocationData.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsLocationData.put("user_id", new TableInfo.Column("user_id", "TEXT", true, 0));
        _columnsLocationData.put("district_id", new TableInfo.Column("district_id", "TEXT", true, 0));
        _columnsLocationData.put("district_name", new TableInfo.Column("district_name", "TEXT", true, 0));
        _columnsLocationData.put("block_id", new TableInfo.Column("block_id", "TEXT", true, 0));
        _columnsLocationData.put("block_name", new TableInfo.Column("block_name", "TEXT", true, 0));
        _columnsLocationData.put("gp_id", new TableInfo.Column("gp_id", "TEXT", true, 0));
        _columnsLocationData.put("gp_name", new TableInfo.Column("gp_name", "TEXT", true, 0));
        _columnsLocationData.put("project_id", new TableInfo.Column("project_id", "TEXT", true, 0));
        _columnsLocationData.put("project_name", new TableInfo.Column("project_name", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysLocationData = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesLocationData = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoLocationData = new TableInfo("LocationData", _columnsLocationData, _foreignKeysLocationData, _indicesLocationData);
        final TableInfo _existingLocationData = TableInfo.read(_db, "LocationData");
        if (! _infoLocationData.equals(_existingLocationData)) {
          throw new IllegalStateException("Migration didn't properly handle LocationData(org.horizen.waterconservation.Models.LocationData).\n"
                  + " Expected:\n" + _infoLocationData + "\n"
                  + " Found:\n" + _existingLocationData);
        }
        final HashMap<String, TableInfo.Column> _columnsProblemData = new HashMap<String, TableInfo.Column>(3);
        _columnsProblemData.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsProblemData.put("problem", new TableInfo.Column("problem", "TEXT", true, 0));
        _columnsProblemData.put("prioroty", new TableInfo.Column("prioroty", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysProblemData = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesProblemData = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoProblemData = new TableInfo("ProblemData", _columnsProblemData, _foreignKeysProblemData, _indicesProblemData);
        final TableInfo _existingProblemData = TableInfo.read(_db, "ProblemData");
        if (! _infoProblemData.equals(_existingProblemData)) {
          throw new IllegalStateException("Migration didn't properly handle ProblemData(org.horizen.waterconservation.Models.ProblemData).\n"
                  + " Expected:\n" + _infoProblemData + "\n"
                  + " Found:\n" + _existingProblemData);
        }
        final HashMap<String, TableInfo.Column> _columnsContactData = new HashMap<String, TableInfo.Column>(4);
        _columnsContactData.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsContactData.put("receiver_name", new TableInfo.Column("receiver_name", "TEXT", true, 0));
        _columnsContactData.put("receiver_designation", new TableInfo.Column("receiver_designation", "TEXT", true, 0));
        _columnsContactData.put("receiver_phno", new TableInfo.Column("receiver_phno", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysContactData = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesContactData = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoContactData = new TableInfo("ContactData", _columnsContactData, _foreignKeysContactData, _indicesContactData);
        final TableInfo _existingContactData = TableInfo.read(_db, "ContactData");
        if (! _infoContactData.equals(_existingContactData)) {
          throw new IllegalStateException("Migration didn't properly handle ContactData(org.horizen.waterconservation.Models.ContactData).\n"
                  + " Expected:\n" + _infoContactData + "\n"
                  + " Found:\n" + _existingContactData);
        }
        final HashMap<String, TableInfo.Column> _columnsProblemLocation = new HashMap<String, TableInfo.Column>(15);
        _columnsProblemLocation.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsProblemLocation.put("user_id", new TableInfo.Column("user_id", "TEXT", true, 0));
        _columnsProblemLocation.put("pipiline_type", new TableInfo.Column("pipiline_type", "TEXT", true, 0));
        _columnsProblemLocation.put("gp_id", new TableInfo.Column("gp_id", "TEXT", true, 0));
        _columnsProblemLocation.put("zone_no", new TableInfo.Column("zone_no", "TEXT", true, 0));
        _columnsProblemLocation.put("mouza_name", new TableInfo.Column("mouza_name", "TEXT", true, 0));
        _columnsProblemLocation.put("problem_id", new TableInfo.Column("problem_id", "TEXT", true, 0));
        _columnsProblemLocation.put("problem_lat", new TableInfo.Column("problem_lat", "TEXT", true, 0));
        _columnsProblemLocation.put("problem_lon", new TableInfo.Column("problem_lon", "TEXT", true, 0));
        _columnsProblemLocation.put("filePath", new TableInfo.Column("filePath", "TEXT", true, 0));
        _columnsProblemLocation.put("resolved", new TableInfo.Column("resolved", "INTEGER", true, 0));
        _columnsProblemLocation.put("remarks", new TableInfo.Column("remarks", "TEXT", true, 0));
        _columnsProblemLocation.put("uploaded", new TableInfo.Column("uploaded", "INTEGER", true, 0));
        _columnsProblemLocation.put("date", new TableInfo.Column("date", "TEXT", true, 0));
        _columnsProblemLocation.put("time", new TableInfo.Column("time", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysProblemLocation = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesProblemLocation = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoProblemLocation = new TableInfo("ProblemLocation", _columnsProblemLocation, _foreignKeysProblemLocation, _indicesProblemLocation);
        final TableInfo _existingProblemLocation = TableInfo.read(_db, "ProblemLocation");
        if (! _infoProblemLocation.equals(_existingProblemLocation)) {
          throw new IllegalStateException("Migration didn't properly handle ProblemLocation(org.horizen.waterconservation.Models.ProblemLocation).\n"
                  + " Expected:\n" + _infoProblemLocation + "\n"
                  + " Found:\n" + _existingProblemLocation);
        }
        final HashMap<String, TableInfo.Column> _columnsCustomMapLayer = new HashMap<String, TableInfo.Column>(3);
        _columnsCustomMapLayer.put("key", new TableInfo.Column("key", "TEXT", true, 1));
        _columnsCustomMapLayer.put("feature", new TableInfo.Column("feature", "TEXT", true, 0));
        _columnsCustomMapLayer.put("visible", new TableInfo.Column("visible", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCustomMapLayer = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCustomMapLayer = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoCustomMapLayer = new TableInfo("CustomMapLayer", _columnsCustomMapLayer, _foreignKeysCustomMapLayer, _indicesCustomMapLayer);
        final TableInfo _existingCustomMapLayer = TableInfo.read(_db, "CustomMapLayer");
        if (! _infoCustomMapLayer.equals(_existingCustomMapLayer)) {
          throw new IllegalStateException("Migration didn't properly handle CustomMapLayer(org.horizen.waterconservation.Models.CustomMapLayer).\n"
                  + " Expected:\n" + _infoCustomMapLayer + "\n"
                  + " Found:\n" + _existingCustomMapLayer);
        }
      }
    }, "56571c9c89c948e5250c8a00b78d2ccd", "bcce78a8522c3738702356873142cddb");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "User","LocationData","ProblemData","ContactData","ProblemLocation","CustomMapLayer");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `User`");
      _db.execSQL("DELETE FROM `LocationData`");
      _db.execSQL("DELETE FROM `ProblemData`");
      _db.execSQL("DELETE FROM `ContactData`");
      _db.execSQL("DELETE FROM `ProblemLocation`");
      _db.execSQL("DELETE FROM `CustomMapLayer`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public UserDao userDao() {
    if (_userDao != null) {
      return _userDao;
    } else {
      synchronized(this) {
        if(_userDao == null) {
          _userDao = new UserDao_Impl(this);
        }
        return _userDao;
      }
    }
  }

  @Override
  public LocationDao locationDataDao() {
    if (_locationDao != null) {
      return _locationDao;
    } else {
      synchronized(this) {
        if(_locationDao == null) {
          _locationDao = new LocationDao_Impl(this);
        }
        return _locationDao;
      }
    }
  }

  @Override
  public ProblemDao problemDataDao() {
    if (_problemDao != null) {
      return _problemDao;
    } else {
      synchronized(this) {
        if(_problemDao == null) {
          _problemDao = new ProblemDao_Impl(this);
        }
        return _problemDao;
      }
    }
  }

  @Override
  public ContactDao contactDataDao() {
    if (_contactDao != null) {
      return _contactDao;
    } else {
      synchronized(this) {
        if(_contactDao == null) {
          _contactDao = new ContactDao_Impl(this);
        }
        return _contactDao;
      }
    }
  }

  @Override
  public ProblemLocationDao problemLocationDao() {
    if (_problemLocationDao != null) {
      return _problemLocationDao;
    } else {
      synchronized(this) {
        if(_problemLocationDao == null) {
          _problemLocationDao = new ProblemLocationDao_Impl(this);
        }
        return _problemLocationDao;
      }
    }
  }

  @Override
  public CustomMapLayerDao customMapLayerDao() {
    if (_customMapLayerDao != null) {
      return _customMapLayerDao;
    } else {
      synchronized(this) {
        if(_customMapLayerDao == null) {
          _customMapLayerDao = new CustomMapLayerDao_Impl(this);
        }
        return _customMapLayerDao;
      }
    }
  }
}
