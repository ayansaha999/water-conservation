package org.horizen.waterconservation.Daos;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.horizen.waterconservation.Helper.DataTypeConverter;
import org.horizen.waterconservation.Models.User;

@SuppressWarnings("unchecked")
public class UserDao_Impl implements UserDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfUser;

  private final DataTypeConverter __dataTypeConverter = new DataTypeConverter();

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfUser;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfUser;

  public UserDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfUser = new EntityInsertionAdapter<User>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `User`(`user_id`,`user_name`,`password`,`file_path`,`version`,`loggedIn`,`location`) VALUES (?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, User value) {
        if (value.getUser_id() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getUser_id());
        }
        if (value.getUser_name() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getUser_name());
        }
        if (value.getPassword() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getPassword());
        }
        if (value.getFile_path() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getFile_path());
        }
        if (value.getVersion() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getVersion());
        }
        final int _tmp;
        _tmp = value.getLoggedIn() ? 1 : 0;
        stmt.bindLong(6, _tmp);
        final String _tmp_1;
        _tmp_1 = __dataTypeConverter.fromLatLngToString(value.getLocation());
        if (_tmp_1 == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, _tmp_1);
        }
      }
    };
    this.__deletionAdapterOfUser = new EntityDeletionOrUpdateAdapter<User>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `User` WHERE `user_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, User value) {
        if (value.getUser_id() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getUser_id());
        }
      }
    };
    this.__updateAdapterOfUser = new EntityDeletionOrUpdateAdapter<User>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR REPLACE `User` SET `user_id` = ?,`user_name` = ?,`password` = ?,`file_path` = ?,`version` = ?,`loggedIn` = ?,`location` = ? WHERE `user_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, User value) {
        if (value.getUser_id() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getUser_id());
        }
        if (value.getUser_name() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getUser_name());
        }
        if (value.getPassword() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getPassword());
        }
        if (value.getFile_path() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getFile_path());
        }
        if (value.getVersion() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getVersion());
        }
        final int _tmp;
        _tmp = value.getLoggedIn() ? 1 : 0;
        stmt.bindLong(6, _tmp);
        final String _tmp_1;
        _tmp_1 = __dataTypeConverter.fromLatLngToString(value.getLocation());
        if (_tmp_1 == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, _tmp_1);
        }
        if (value.getUser_id() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getUser_id());
        }
      }
    };
  }

  @Override
  public void insertUser(User... User) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfUser.insert(User);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteUser(User User) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfUser.handle(User);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateUser(User... User) {
    __db.beginTransaction();
    try {
      __updateAdapterOfUser.handleMultiple(User);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<User>> getAllUsers() {
    final String _sql = "select * from User order by user_name";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<User>>() {
      private Observer _observer;

      @Override
      protected List<User> compute() {
        if (_observer == null) {
          _observer = new Observer("User") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
          final int _cursorIndexOfUserName = _cursor.getColumnIndexOrThrow("user_name");
          final int _cursorIndexOfPassword = _cursor.getColumnIndexOrThrow("password");
          final int _cursorIndexOfFilePath = _cursor.getColumnIndexOrThrow("file_path");
          final int _cursorIndexOfVersion = _cursor.getColumnIndexOrThrow("version");
          final int _cursorIndexOfLoggedIn = _cursor.getColumnIndexOrThrow("loggedIn");
          final int _cursorIndexOfLocation = _cursor.getColumnIndexOrThrow("location");
          final List<User> _result = new ArrayList<User>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final User _item;
            _item = new User();
            final String _tmpUser_id;
            _tmpUser_id = _cursor.getString(_cursorIndexOfUserId);
            _item.setUser_id(_tmpUser_id);
            final String _tmpUser_name;
            _tmpUser_name = _cursor.getString(_cursorIndexOfUserName);
            _item.setUser_name(_tmpUser_name);
            final String _tmpPassword;
            _tmpPassword = _cursor.getString(_cursorIndexOfPassword);
            _item.setPassword(_tmpPassword);
            final String _tmpFile_path;
            _tmpFile_path = _cursor.getString(_cursorIndexOfFilePath);
            _item.setFile_path(_tmpFile_path);
            final String _tmpVersion;
            _tmpVersion = _cursor.getString(_cursorIndexOfVersion);
            _item.setVersion(_tmpVersion);
            final boolean _tmpLoggedIn;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfLoggedIn);
            _tmpLoggedIn = _tmp != 0;
            _item.setLoggedIn(_tmpLoggedIn);
            final LatLng _tmpLocation;
            final String _tmp_1;
            _tmp_1 = _cursor.getString(_cursorIndexOfLocation);
            _tmpLocation = __dataTypeConverter.fromStringToLatLng(_tmp_1);
            _item.setLocation(_tmpLocation);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public User findUserById(String id) {
    final String _sql = "select * from User where user_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
      final int _cursorIndexOfUserName = _cursor.getColumnIndexOrThrow("user_name");
      final int _cursorIndexOfPassword = _cursor.getColumnIndexOrThrow("password");
      final int _cursorIndexOfFilePath = _cursor.getColumnIndexOrThrow("file_path");
      final int _cursorIndexOfVersion = _cursor.getColumnIndexOrThrow("version");
      final int _cursorIndexOfLoggedIn = _cursor.getColumnIndexOrThrow("loggedIn");
      final int _cursorIndexOfLocation = _cursor.getColumnIndexOrThrow("location");
      final User _result;
      if(_cursor.moveToFirst()) {
        _result = new User();
        final String _tmpUser_id;
        _tmpUser_id = _cursor.getString(_cursorIndexOfUserId);
        _result.setUser_id(_tmpUser_id);
        final String _tmpUser_name;
        _tmpUser_name = _cursor.getString(_cursorIndexOfUserName);
        _result.setUser_name(_tmpUser_name);
        final String _tmpPassword;
        _tmpPassword = _cursor.getString(_cursorIndexOfPassword);
        _result.setPassword(_tmpPassword);
        final String _tmpFile_path;
        _tmpFile_path = _cursor.getString(_cursorIndexOfFilePath);
        _result.setFile_path(_tmpFile_path);
        final String _tmpVersion;
        _tmpVersion = _cursor.getString(_cursorIndexOfVersion);
        _result.setVersion(_tmpVersion);
        final boolean _tmpLoggedIn;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfLoggedIn);
        _tmpLoggedIn = _tmp != 0;
        _result.setLoggedIn(_tmpLoggedIn);
        final LatLng _tmpLocation;
        final String _tmp_1;
        _tmp_1 = _cursor.getString(_cursorIndexOfLocation);
        _tmpLocation = __dataTypeConverter.fromStringToLatLng(_tmp_1);
        _result.setLocation(_tmpLocation);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public LiveData<LatLng> findUserLocation(String id) {
    final String _sql = "select location from User where user_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    return new ComputableLiveData<LatLng>() {
      private Observer _observer;

      @Override
      protected LatLng compute() {
        if (_observer == null) {
          _observer = new Observer("User") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final LatLng _result;
          if(_cursor.moveToFirst()) {
            final String _tmp;
            _tmp = _cursor.getString(0);
            _result = __dataTypeConverter.fromStringToLatLng(_tmp);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }
}
