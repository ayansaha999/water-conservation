package org.horizen.waterconservation.Daos;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import in.net.freak.androidcore.Map.GeoJson.MapLayerFeature;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import org.horizen.waterconservation.Helper.DataTypeConverter;
import org.horizen.waterconservation.Models.CustomMapLayer;

@SuppressWarnings("unchecked")
public class CustomMapLayerDao_Impl implements CustomMapLayerDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfCustomMapLayer;

  private final DataTypeConverter __dataTypeConverter = new DataTypeConverter();

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfCustomMapLayer;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfCustomMapLayer;

  public CustomMapLayerDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfCustomMapLayer = new EntityInsertionAdapter<CustomMapLayer>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `CustomMapLayer`(`key`,`feature`,`visible`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CustomMapLayer value) {
        if (value.getKey() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getKey());
        }
        final String _tmp;
        _tmp = __dataTypeConverter.fromArrayMapFeatureToString(value.getFeature());
        if (_tmp == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, _tmp);
        }
        final int _tmp_1;
        _tmp_1 = value.getVisible() ? 1 : 0;
        stmt.bindLong(3, _tmp_1);
      }
    };
    this.__deletionAdapterOfCustomMapLayer = new EntityDeletionOrUpdateAdapter<CustomMapLayer>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `CustomMapLayer` WHERE `key` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CustomMapLayer value) {
        if (value.getKey() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getKey());
        }
      }
    };
    this.__updateAdapterOfCustomMapLayer = new EntityDeletionOrUpdateAdapter<CustomMapLayer>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR REPLACE `CustomMapLayer` SET `key` = ?,`feature` = ?,`visible` = ? WHERE `key` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CustomMapLayer value) {
        if (value.getKey() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getKey());
        }
        final String _tmp;
        _tmp = __dataTypeConverter.fromArrayMapFeatureToString(value.getFeature());
        if (_tmp == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, _tmp);
        }
        final int _tmp_1;
        _tmp_1 = value.getVisible() ? 1 : 0;
        stmt.bindLong(3, _tmp_1);
        if (value.getKey() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getKey());
        }
      }
    };
  }

  @Override
  public void insertCustomMapLayer(CustomMapLayer... CustomMapLayer) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfCustomMapLayer.insert(CustomMapLayer);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteCustomMapLayer(CustomMapLayer CustomMapLayer) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfCustomMapLayer.handle(CustomMapLayer);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateCustomMapLayer(CustomMapLayer... CustomMapLayer) {
    __db.beginTransaction();
    try {
      __updateAdapterOfCustomMapLayer.handleMultiple(CustomMapLayer);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<CustomMapLayer> getAllCustomMapLayers() {
    final String _sql = "select * from CustomMapLayer";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfKey = _cursor.getColumnIndexOrThrow("key");
      final int _cursorIndexOfFeature = _cursor.getColumnIndexOrThrow("feature");
      final int _cursorIndexOfVisible = _cursor.getColumnIndexOrThrow("visible");
      final List<CustomMapLayer> _result = new ArrayList<CustomMapLayer>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final CustomMapLayer _item;
        _item = new CustomMapLayer();
        final String _tmpKey;
        _tmpKey = _cursor.getString(_cursorIndexOfKey);
        _item.setKey(_tmpKey);
        final ArrayList<MapLayerFeature> _tmpFeature;
        final String _tmp;
        _tmp = _cursor.getString(_cursorIndexOfFeature);
        _tmpFeature = __dataTypeConverter.fromStringToArrayMapFeature(_tmp);
        _item.setFeature(_tmpFeature);
        final boolean _tmpVisible;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfVisible);
        _tmpVisible = _tmp_1 != 0;
        _item.setVisible(_tmpVisible);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
