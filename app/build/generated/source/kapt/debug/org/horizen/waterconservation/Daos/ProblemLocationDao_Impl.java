package org.horizen.waterconservation.Daos;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import android.support.annotation.NonNull;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.horizen.waterconservation.Models.ProblemLocation;

@SuppressWarnings("unchecked")
public class ProblemLocationDao_Impl implements ProblemLocationDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfProblemLocation;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfProblemLocation;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfProblemLocation;

  public ProblemLocationDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfProblemLocation = new EntityInsertionAdapter<ProblemLocation>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `ProblemLocation`(`id`,`user_id`,`pipiline_type`,`gp_id`,`zone_no`,`mouza_name`,`problem_id`,`problem_lat`,`problem_lon`,`filePath`,`resolved`,`remarks`,`uploaded`,`date`,`time`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProblemLocation value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getUser_id() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getUser_id());
        }
        if (value.getPipiline_type() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getPipiline_type());
        }
        if (value.getGp_id() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getGp_id());
        }
        if (value.getZone_no() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getZone_no());
        }
        if (value.getMouza_name() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getMouza_name());
        }
        if (value.getProblem_id() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getProblem_id());
        }
        if (value.getProblem_lat() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getProblem_lat());
        }
        if (value.getProblem_lon() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getProblem_lon());
        }
        if (value.getFilePath() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getFilePath());
        }
        final int _tmp;
        _tmp = value.getResolved() ? 1 : 0;
        stmt.bindLong(11, _tmp);
        if (value.getRemarks() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getRemarks());
        }
        final int _tmp_1;
        _tmp_1 = value.getUploaded() ? 1 : 0;
        stmt.bindLong(13, _tmp_1);
        if (value.getDate() == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.getDate());
        }
        if (value.getTime() == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.getTime());
        }
      }
    };
    this.__deletionAdapterOfProblemLocation = new EntityDeletionOrUpdateAdapter<ProblemLocation>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `ProblemLocation` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProblemLocation value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
      }
    };
    this.__updateAdapterOfProblemLocation = new EntityDeletionOrUpdateAdapter<ProblemLocation>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR REPLACE `ProblemLocation` SET `id` = ?,`user_id` = ?,`pipiline_type` = ?,`gp_id` = ?,`zone_no` = ?,`mouza_name` = ?,`problem_id` = ?,`problem_lat` = ?,`problem_lon` = ?,`filePath` = ?,`resolved` = ?,`remarks` = ?,`uploaded` = ?,`date` = ?,`time` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProblemLocation value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getUser_id() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getUser_id());
        }
        if (value.getPipiline_type() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getPipiline_type());
        }
        if (value.getGp_id() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getGp_id());
        }
        if (value.getZone_no() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getZone_no());
        }
        if (value.getMouza_name() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getMouza_name());
        }
        if (value.getProblem_id() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getProblem_id());
        }
        if (value.getProblem_lat() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getProblem_lat());
        }
        if (value.getProblem_lon() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getProblem_lon());
        }
        if (value.getFilePath() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getFilePath());
        }
        final int _tmp;
        _tmp = value.getResolved() ? 1 : 0;
        stmt.bindLong(11, _tmp);
        if (value.getRemarks() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getRemarks());
        }
        final int _tmp_1;
        _tmp_1 = value.getUploaded() ? 1 : 0;
        stmt.bindLong(13, _tmp_1);
        if (value.getDate() == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.getDate());
        }
        if (value.getTime() == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.getTime());
        }
        if (value.getId() == null) {
          stmt.bindNull(16);
        } else {
          stmt.bindString(16, value.getId());
        }
      }
    };
  }

  @Override
  public void insertProblemLocation(ProblemLocation... ProblemLocation) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfProblemLocation.insert(ProblemLocation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteProblemLocation(ProblemLocation ProblemLocation) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfProblemLocation.handle(ProblemLocation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateProblemLocation(ProblemLocation... ProblemLocation) {
    __db.beginTransaction();
    try {
      __updateAdapterOfProblemLocation.handleMultiple(ProblemLocation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<ProblemLocation>> getAllProblemLocations() {
    final String _sql = "select * from ProblemLocation";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<ProblemLocation>>() {
      private Observer _observer;

      @Override
      protected List<ProblemLocation> compute() {
        if (_observer == null) {
          _observer = new Observer("ProblemLocation") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
          final int _cursorIndexOfPipilineType = _cursor.getColumnIndexOrThrow("pipiline_type");
          final int _cursorIndexOfGpId = _cursor.getColumnIndexOrThrow("gp_id");
          final int _cursorIndexOfZoneNo = _cursor.getColumnIndexOrThrow("zone_no");
          final int _cursorIndexOfMouzaName = _cursor.getColumnIndexOrThrow("mouza_name");
          final int _cursorIndexOfProblemId = _cursor.getColumnIndexOrThrow("problem_id");
          final int _cursorIndexOfProblemLat = _cursor.getColumnIndexOrThrow("problem_lat");
          final int _cursorIndexOfProblemLon = _cursor.getColumnIndexOrThrow("problem_lon");
          final int _cursorIndexOfFilePath = _cursor.getColumnIndexOrThrow("filePath");
          final int _cursorIndexOfResolved = _cursor.getColumnIndexOrThrow("resolved");
          final int _cursorIndexOfRemarks = _cursor.getColumnIndexOrThrow("remarks");
          final int _cursorIndexOfUploaded = _cursor.getColumnIndexOrThrow("uploaded");
          final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
          final int _cursorIndexOfTime = _cursor.getColumnIndexOrThrow("time");
          final List<ProblemLocation> _result = new ArrayList<ProblemLocation>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ProblemLocation _item;
            _item = new ProblemLocation();
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            _item.setId(_tmpId);
            final String _tmpUser_id;
            _tmpUser_id = _cursor.getString(_cursorIndexOfUserId);
            _item.setUser_id(_tmpUser_id);
            final String _tmpPipiline_type;
            _tmpPipiline_type = _cursor.getString(_cursorIndexOfPipilineType);
            _item.setPipiline_type(_tmpPipiline_type);
            final String _tmpGp_id;
            _tmpGp_id = _cursor.getString(_cursorIndexOfGpId);
            _item.setGp_id(_tmpGp_id);
            final String _tmpZone_no;
            _tmpZone_no = _cursor.getString(_cursorIndexOfZoneNo);
            _item.setZone_no(_tmpZone_no);
            final String _tmpMouza_name;
            _tmpMouza_name = _cursor.getString(_cursorIndexOfMouzaName);
            _item.setMouza_name(_tmpMouza_name);
            final String _tmpProblem_id;
            _tmpProblem_id = _cursor.getString(_cursorIndexOfProblemId);
            _item.setProblem_id(_tmpProblem_id);
            final String _tmpProblem_lat;
            _tmpProblem_lat = _cursor.getString(_cursorIndexOfProblemLat);
            _item.setProblem_lat(_tmpProblem_lat);
            final String _tmpProblem_lon;
            _tmpProblem_lon = _cursor.getString(_cursorIndexOfProblemLon);
            _item.setProblem_lon(_tmpProblem_lon);
            final String _tmpFilePath;
            _tmpFilePath = _cursor.getString(_cursorIndexOfFilePath);
            _item.setFilePath(_tmpFilePath);
            final boolean _tmpResolved;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfResolved);
            _tmpResolved = _tmp != 0;
            _item.setResolved(_tmpResolved);
            final String _tmpRemarks;
            _tmpRemarks = _cursor.getString(_cursorIndexOfRemarks);
            _item.setRemarks(_tmpRemarks);
            final boolean _tmpUploaded;
            final int _tmp_1;
            _tmp_1 = _cursor.getInt(_cursorIndexOfUploaded);
            _tmpUploaded = _tmp_1 != 0;
            _item.setUploaded(_tmpUploaded);
            final String _tmpDate;
            _tmpDate = _cursor.getString(_cursorIndexOfDate);
            _item.setDate(_tmpDate);
            final String _tmpTime;
            _tmpTime = _cursor.getString(_cursorIndexOfTime);
            _item.setTime(_tmpTime);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public ProblemLocation findProblemLocationById(String id) {
    final String _sql = "select * from ProblemLocation where id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
      final int _cursorIndexOfPipilineType = _cursor.getColumnIndexOrThrow("pipiline_type");
      final int _cursorIndexOfGpId = _cursor.getColumnIndexOrThrow("gp_id");
      final int _cursorIndexOfZoneNo = _cursor.getColumnIndexOrThrow("zone_no");
      final int _cursorIndexOfMouzaName = _cursor.getColumnIndexOrThrow("mouza_name");
      final int _cursorIndexOfProblemId = _cursor.getColumnIndexOrThrow("problem_id");
      final int _cursorIndexOfProblemLat = _cursor.getColumnIndexOrThrow("problem_lat");
      final int _cursorIndexOfProblemLon = _cursor.getColumnIndexOrThrow("problem_lon");
      final int _cursorIndexOfFilePath = _cursor.getColumnIndexOrThrow("filePath");
      final int _cursorIndexOfResolved = _cursor.getColumnIndexOrThrow("resolved");
      final int _cursorIndexOfRemarks = _cursor.getColumnIndexOrThrow("remarks");
      final int _cursorIndexOfUploaded = _cursor.getColumnIndexOrThrow("uploaded");
      final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
      final int _cursorIndexOfTime = _cursor.getColumnIndexOrThrow("time");
      final ProblemLocation _result;
      if(_cursor.moveToFirst()) {
        _result = new ProblemLocation();
        final String _tmpId;
        _tmpId = _cursor.getString(_cursorIndexOfId);
        _result.setId(_tmpId);
        final String _tmpUser_id;
        _tmpUser_id = _cursor.getString(_cursorIndexOfUserId);
        _result.setUser_id(_tmpUser_id);
        final String _tmpPipiline_type;
        _tmpPipiline_type = _cursor.getString(_cursorIndexOfPipilineType);
        _result.setPipiline_type(_tmpPipiline_type);
        final String _tmpGp_id;
        _tmpGp_id = _cursor.getString(_cursorIndexOfGpId);
        _result.setGp_id(_tmpGp_id);
        final String _tmpZone_no;
        _tmpZone_no = _cursor.getString(_cursorIndexOfZoneNo);
        _result.setZone_no(_tmpZone_no);
        final String _tmpMouza_name;
        _tmpMouza_name = _cursor.getString(_cursorIndexOfMouzaName);
        _result.setMouza_name(_tmpMouza_name);
        final String _tmpProblem_id;
        _tmpProblem_id = _cursor.getString(_cursorIndexOfProblemId);
        _result.setProblem_id(_tmpProblem_id);
        final String _tmpProblem_lat;
        _tmpProblem_lat = _cursor.getString(_cursorIndexOfProblemLat);
        _result.setProblem_lat(_tmpProblem_lat);
        final String _tmpProblem_lon;
        _tmpProblem_lon = _cursor.getString(_cursorIndexOfProblemLon);
        _result.setProblem_lon(_tmpProblem_lon);
        final String _tmpFilePath;
        _tmpFilePath = _cursor.getString(_cursorIndexOfFilePath);
        _result.setFilePath(_tmpFilePath);
        final boolean _tmpResolved;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfResolved);
        _tmpResolved = _tmp != 0;
        _result.setResolved(_tmpResolved);
        final String _tmpRemarks;
        _tmpRemarks = _cursor.getString(_cursorIndexOfRemarks);
        _result.setRemarks(_tmpRemarks);
        final boolean _tmpUploaded;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfUploaded);
        _tmpUploaded = _tmp_1 != 0;
        _result.setUploaded(_tmpUploaded);
        final String _tmpDate;
        _tmpDate = _cursor.getString(_cursorIndexOfDate);
        _result.setDate(_tmpDate);
        final String _tmpTime;
        _tmpTime = _cursor.getString(_cursorIndexOfTime);
        _result.setTime(_tmpTime);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
