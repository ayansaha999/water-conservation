package org.horizen.waterconservation.Daos;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import android.support.annotation.NonNull;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.horizen.waterconservation.Models.ContactData;

@SuppressWarnings("unchecked")
public class ContactDao_Impl implements ContactDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfContactData;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfContactData;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfContactData;

  public ContactDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfContactData = new EntityInsertionAdapter<ContactData>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `ContactData`(`id`,`receiver_name`,`receiver_designation`,`receiver_phno`) VALUES (?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ContactData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getReceiver_name() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getReceiver_name());
        }
        if (value.getReceiver_designation() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getReceiver_designation());
        }
        if (value.getReceiver_phno() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getReceiver_phno());
        }
      }
    };
    this.__deletionAdapterOfContactData = new EntityDeletionOrUpdateAdapter<ContactData>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `ContactData` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ContactData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
      }
    };
    this.__updateAdapterOfContactData = new EntityDeletionOrUpdateAdapter<ContactData>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR REPLACE `ContactData` SET `id` = ?,`receiver_name` = ?,`receiver_designation` = ?,`receiver_phno` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ContactData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getReceiver_name() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getReceiver_name());
        }
        if (value.getReceiver_designation() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getReceiver_designation());
        }
        if (value.getReceiver_phno() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getReceiver_phno());
        }
        if (value.getId() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getId());
        }
      }
    };
  }

  @Override
  public void insertContactData(ContactData... ContactData) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfContactData.insert(ContactData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteContactData(ContactData ContactData) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfContactData.handle(ContactData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateContactData(ContactData... ContactData) {
    __db.beginTransaction();
    try {
      __updateAdapterOfContactData.handleMultiple(ContactData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<ContactData>> getAllContactDatas() {
    final String _sql = "select * from ContactData order by receiver_name";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<ContactData>>() {
      private Observer _observer;

      @Override
      protected List<ContactData> compute() {
        if (_observer == null) {
          _observer = new Observer("ContactData") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfReceiverName = _cursor.getColumnIndexOrThrow("receiver_name");
          final int _cursorIndexOfReceiverDesignation = _cursor.getColumnIndexOrThrow("receiver_designation");
          final int _cursorIndexOfReceiverPhno = _cursor.getColumnIndexOrThrow("receiver_phno");
          final List<ContactData> _result = new ArrayList<ContactData>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ContactData _item;
            _item = new ContactData();
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            _item.setId(_tmpId);
            final String _tmpReceiver_name;
            _tmpReceiver_name = _cursor.getString(_cursorIndexOfReceiverName);
            _item.setReceiver_name(_tmpReceiver_name);
            final String _tmpReceiver_designation;
            _tmpReceiver_designation = _cursor.getString(_cursorIndexOfReceiverDesignation);
            _item.setReceiver_designation(_tmpReceiver_designation);
            final String _tmpReceiver_phno;
            _tmpReceiver_phno = _cursor.getString(_cursorIndexOfReceiverPhno);
            _item.setReceiver_phno(_tmpReceiver_phno);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public ContactData findContactDataById(String id) {
    final String _sql = "select * from ContactData where id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfReceiverName = _cursor.getColumnIndexOrThrow("receiver_name");
      final int _cursorIndexOfReceiverDesignation = _cursor.getColumnIndexOrThrow("receiver_designation");
      final int _cursorIndexOfReceiverPhno = _cursor.getColumnIndexOrThrow("receiver_phno");
      final ContactData _result;
      if(_cursor.moveToFirst()) {
        _result = new ContactData();
        final String _tmpId;
        _tmpId = _cursor.getString(_cursorIndexOfId);
        _result.setId(_tmpId);
        final String _tmpReceiver_name;
        _tmpReceiver_name = _cursor.getString(_cursorIndexOfReceiverName);
        _result.setReceiver_name(_tmpReceiver_name);
        final String _tmpReceiver_designation;
        _tmpReceiver_designation = _cursor.getString(_cursorIndexOfReceiverDesignation);
        _result.setReceiver_designation(_tmpReceiver_designation);
        final String _tmpReceiver_phno;
        _tmpReceiver_phno = _cursor.getString(_cursorIndexOfReceiverPhno);
        _result.setReceiver_phno(_tmpReceiver_phno);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
