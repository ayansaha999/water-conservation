package org.horizen.waterconservation.Daos;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import android.support.annotation.NonNull;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.horizen.waterconservation.Models.ProblemData;

@SuppressWarnings("unchecked")
public class ProblemDao_Impl implements ProblemDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfProblemData;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfProblemData;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfProblemData;

  public ProblemDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfProblemData = new EntityInsertionAdapter<ProblemData>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `ProblemData`(`id`,`problem`,`prioroty`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProblemData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getProblem() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getProblem());
        }
        if (value.getPrioroty() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getPrioroty());
        }
      }
    };
    this.__deletionAdapterOfProblemData = new EntityDeletionOrUpdateAdapter<ProblemData>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `ProblemData` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProblemData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
      }
    };
    this.__updateAdapterOfProblemData = new EntityDeletionOrUpdateAdapter<ProblemData>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR REPLACE `ProblemData` SET `id` = ?,`problem` = ?,`prioroty` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProblemData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getProblem() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getProblem());
        }
        if (value.getPrioroty() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getPrioroty());
        }
        if (value.getId() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getId());
        }
      }
    };
  }

  @Override
  public void insertProblemData(ProblemData... ProblemData) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfProblemData.insert(ProblemData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteProblemData(ProblemData ProblemData) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfProblemData.handle(ProblemData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateProblemData(ProblemData... ProblemData) {
    __db.beginTransaction();
    try {
      __updateAdapterOfProblemData.handleMultiple(ProblemData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<ProblemData>> getAllProblemDatas() {
    final String _sql = "select * from ProblemData order by problem";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<ProblemData>>() {
      private Observer _observer;

      @Override
      protected List<ProblemData> compute() {
        if (_observer == null) {
          _observer = new Observer("ProblemData") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfProblem = _cursor.getColumnIndexOrThrow("problem");
          final int _cursorIndexOfPrioroty = _cursor.getColumnIndexOrThrow("prioroty");
          final List<ProblemData> _result = new ArrayList<ProblemData>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ProblemData _item;
            _item = new ProblemData();
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            _item.setId(_tmpId);
            final String _tmpProblem;
            _tmpProblem = _cursor.getString(_cursorIndexOfProblem);
            _item.setProblem(_tmpProblem);
            final String _tmpPrioroty;
            _tmpPrioroty = _cursor.getString(_cursorIndexOfPrioroty);
            _item.setPrioroty(_tmpPrioroty);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public ProblemData findProblemDataById(String id) {
    final String _sql = "select * from ProblemData where id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfProblem = _cursor.getColumnIndexOrThrow("problem");
      final int _cursorIndexOfPrioroty = _cursor.getColumnIndexOrThrow("prioroty");
      final ProblemData _result;
      if(_cursor.moveToFirst()) {
        _result = new ProblemData();
        final String _tmpId;
        _tmpId = _cursor.getString(_cursorIndexOfId);
        _result.setId(_tmpId);
        final String _tmpProblem;
        _tmpProblem = _cursor.getString(_cursorIndexOfProblem);
        _result.setProblem(_tmpProblem);
        final String _tmpPrioroty;
        _tmpPrioroty = _cursor.getString(_cursorIndexOfPrioroty);
        _result.setPrioroty(_tmpPrioroty);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
