package org.horizen.waterconservation.Daos;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import android.support.annotation.NonNull;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.horizen.waterconservation.Models.LocationData;

@SuppressWarnings("unchecked")
public class LocationDao_Impl implements LocationDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfLocationData;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfLocationData;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfLocationData;

  public LocationDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfLocationData = new EntityInsertionAdapter<LocationData>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `LocationData`(`id`,`user_id`,`district_id`,`district_name`,`block_id`,`block_name`,`gp_id`,`gp_name`,`project_id`,`project_name`) VALUES (?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, LocationData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getUser_id() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getUser_id());
        }
        if (value.getDistrict_id() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getDistrict_id());
        }
        if (value.getDistrict_name() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDistrict_name());
        }
        if (value.getBlock_id() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getBlock_id());
        }
        if (value.getBlock_name() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getBlock_name());
        }
        if (value.getGp_id() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getGp_id());
        }
        if (value.getGp_name() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getGp_name());
        }
        if (value.getProject_id() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getProject_id());
        }
        if (value.getProject_name() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getProject_name());
        }
      }
    };
    this.__deletionAdapterOfLocationData = new EntityDeletionOrUpdateAdapter<LocationData>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `LocationData` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, LocationData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
      }
    };
    this.__updateAdapterOfLocationData = new EntityDeletionOrUpdateAdapter<LocationData>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR REPLACE `LocationData` SET `id` = ?,`user_id` = ?,`district_id` = ?,`district_name` = ?,`block_id` = ?,`block_name` = ?,`gp_id` = ?,`gp_name` = ?,`project_id` = ?,`project_name` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, LocationData value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getUser_id() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getUser_id());
        }
        if (value.getDistrict_id() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getDistrict_id());
        }
        if (value.getDistrict_name() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDistrict_name());
        }
        if (value.getBlock_id() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getBlock_id());
        }
        if (value.getBlock_name() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getBlock_name());
        }
        if (value.getGp_id() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getGp_id());
        }
        if (value.getGp_name() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getGp_name());
        }
        if (value.getProject_id() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getProject_id());
        }
        if (value.getProject_name() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getProject_name());
        }
        if (value.getId() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.getId());
        }
      }
    };
  }

  @Override
  public void insertLocationData(LocationData... LocationData) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfLocationData.insert(LocationData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteLocationData(LocationData LocationData) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfLocationData.handle(LocationData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateLocationData(LocationData... LocationData) {
    __db.beginTransaction();
    try {
      __updateAdapterOfLocationData.handleMultiple(LocationData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<LocationData>> getAllLocationDatas() {
    final String _sql = "select * from LocationData order by project_name";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<LocationData>>() {
      private Observer _observer;

      @Override
      protected List<LocationData> compute() {
        if (_observer == null) {
          _observer = new Observer("LocationData") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
          final int _cursorIndexOfDistrictId = _cursor.getColumnIndexOrThrow("district_id");
          final int _cursorIndexOfDistrictName = _cursor.getColumnIndexOrThrow("district_name");
          final int _cursorIndexOfBlockId = _cursor.getColumnIndexOrThrow("block_id");
          final int _cursorIndexOfBlockName = _cursor.getColumnIndexOrThrow("block_name");
          final int _cursorIndexOfGpId = _cursor.getColumnIndexOrThrow("gp_id");
          final int _cursorIndexOfGpName = _cursor.getColumnIndexOrThrow("gp_name");
          final int _cursorIndexOfProjectId = _cursor.getColumnIndexOrThrow("project_id");
          final int _cursorIndexOfProjectName = _cursor.getColumnIndexOrThrow("project_name");
          final List<LocationData> _result = new ArrayList<LocationData>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final LocationData _item;
            _item = new LocationData();
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            _item.setId(_tmpId);
            final String _tmpUser_id;
            _tmpUser_id = _cursor.getString(_cursorIndexOfUserId);
            _item.setUser_id(_tmpUser_id);
            final String _tmpDistrict_id;
            _tmpDistrict_id = _cursor.getString(_cursorIndexOfDistrictId);
            _item.setDistrict_id(_tmpDistrict_id);
            final String _tmpDistrict_name;
            _tmpDistrict_name = _cursor.getString(_cursorIndexOfDistrictName);
            _item.setDistrict_name(_tmpDistrict_name);
            final String _tmpBlock_id;
            _tmpBlock_id = _cursor.getString(_cursorIndexOfBlockId);
            _item.setBlock_id(_tmpBlock_id);
            final String _tmpBlock_name;
            _tmpBlock_name = _cursor.getString(_cursorIndexOfBlockName);
            _item.setBlock_name(_tmpBlock_name);
            final String _tmpGp_id;
            _tmpGp_id = _cursor.getString(_cursorIndexOfGpId);
            _item.setGp_id(_tmpGp_id);
            final String _tmpGp_name;
            _tmpGp_name = _cursor.getString(_cursorIndexOfGpName);
            _item.setGp_name(_tmpGp_name);
            final String _tmpProject_id;
            _tmpProject_id = _cursor.getString(_cursorIndexOfProjectId);
            _item.setProject_id(_tmpProject_id);
            final String _tmpProject_name;
            _tmpProject_name = _cursor.getString(_cursorIndexOfProjectName);
            _item.setProject_name(_tmpProject_name);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LocationData findLocationDataById(String id) {
    final String _sql = "select * from LocationData where project_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
      final int _cursorIndexOfDistrictId = _cursor.getColumnIndexOrThrow("district_id");
      final int _cursorIndexOfDistrictName = _cursor.getColumnIndexOrThrow("district_name");
      final int _cursorIndexOfBlockId = _cursor.getColumnIndexOrThrow("block_id");
      final int _cursorIndexOfBlockName = _cursor.getColumnIndexOrThrow("block_name");
      final int _cursorIndexOfGpId = _cursor.getColumnIndexOrThrow("gp_id");
      final int _cursorIndexOfGpName = _cursor.getColumnIndexOrThrow("gp_name");
      final int _cursorIndexOfProjectId = _cursor.getColumnIndexOrThrow("project_id");
      final int _cursorIndexOfProjectName = _cursor.getColumnIndexOrThrow("project_name");
      final LocationData _result;
      if(_cursor.moveToFirst()) {
        _result = new LocationData();
        final String _tmpId;
        _tmpId = _cursor.getString(_cursorIndexOfId);
        _result.setId(_tmpId);
        final String _tmpUser_id;
        _tmpUser_id = _cursor.getString(_cursorIndexOfUserId);
        _result.setUser_id(_tmpUser_id);
        final String _tmpDistrict_id;
        _tmpDistrict_id = _cursor.getString(_cursorIndexOfDistrictId);
        _result.setDistrict_id(_tmpDistrict_id);
        final String _tmpDistrict_name;
        _tmpDistrict_name = _cursor.getString(_cursorIndexOfDistrictName);
        _result.setDistrict_name(_tmpDistrict_name);
        final String _tmpBlock_id;
        _tmpBlock_id = _cursor.getString(_cursorIndexOfBlockId);
        _result.setBlock_id(_tmpBlock_id);
        final String _tmpBlock_name;
        _tmpBlock_name = _cursor.getString(_cursorIndexOfBlockName);
        _result.setBlock_name(_tmpBlock_name);
        final String _tmpGp_id;
        _tmpGp_id = _cursor.getString(_cursorIndexOfGpId);
        _result.setGp_id(_tmpGp_id);
        final String _tmpGp_name;
        _tmpGp_name = _cursor.getString(_cursorIndexOfGpName);
        _result.setGp_name(_tmpGp_name);
        final String _tmpProject_id;
        _tmpProject_id = _cursor.getString(_cursorIndexOfProjectId);
        _result.setProject_id(_tmpProject_id);
        final String _tmpProject_name;
        _tmpProject_name = _cursor.getString(_cursorIndexOfProjectName);
        _result.setProject_name(_tmpProject_name);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
