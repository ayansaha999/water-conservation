package org.horizen.waterconservation.Auth

import `in`.net.freak.androidcore.ValuePair
import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.activity_auth.*
import org.horizen.waterconservation.Helper.DataTypeConverter
import org.horizen.waterconservation.Helper.Keys
import org.horizen.waterconservation.Home.HomeActivity

import org.horizen.waterconservation.R
import org.horizen.waterconservation.Root.RootActivity
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.util.ArrayList
import java.util.concurrent.TimeUnit

class AuthActivity : RootActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_auth)

        core.appUtility.checkPermissions(this@AuthActivity, permissions, Keys.REQUEST_CHECK_PERMISSION)

        if (prefs.getBoolean(Keys.LOGGED)) {
            startActivity(Intent(this@AuthActivity, HomeActivity::class.java))
            finish()
        }

        rootVm.authState.observe(this, Observer { value ->
            if (value != null) {
                when (value) {
                    Keys.LOGGED_OUT -> {
                        username.visibility = View.VISIBLE
                        password.visibility = View.VISIBLE
                        loginProgress.visibility = View.GONE
                        loginButton.visibility = View.VISIBLE
                    }
                    Keys.LOGGING_IN -> {
                        username.visibility = View.VISIBLE
                        password.visibility = View.VISIBLE
                        loginProgress.visibility = View.VISIBLE
                        loginButton.visibility = View.GONE
                        rootVm.authStateMessage.value = "Checking user data..."
                    }
                    Keys.LOGGED_IN -> {
                        username.visibility = View.GONE
                        password.visibility = View.GONE
                        loginProgress.visibility = View.GONE
                        loginButton.visibility = View.GONE
                        rootVm.authStateMessage.value = "Syncing user data..."
                        progressStatusText.visibility = View.VISIBLE
                        progressStatus.visibility = View.VISIBLE
                        completeProgress(progressBar1)
                    }
                    Keys.DOWNLOADING_DATA -> {
                        username.visibility = View.GONE
                        password.visibility = View.GONE
                        loginProgress.visibility = View.GONE
                        loginButton.visibility = View.GONE
                    }
                    Keys.DOWNLOADED_DATA -> {
                        username.visibility = View.GONE
                        password.visibility = View.GONE
                        loginProgress.visibility = View.GONE
                        loginButton.visibility = View.GONE
                        completeProgress(progressBar2)
                    }
                }
            }
        })

        rootVm.authStateMessage.observe(this, Observer { message ->
            if (!message.isNullOrEmpty()) {
                progressStatusText.text = message
            }
        })

        loginButton.setOnClickListener {
            rootVm.authState.value = Keys.LOGGING_IN
            login(username.text.toString(), password.text.toString())
        }
    }


    fun login(username: String, password: String) {
        doAsync {
            try {
                val valuePairs = ArrayList<ValuePair>()
                valuePairs.add(ValuePair("uname", username))
                valuePairs.add(ValuePair("password", password))

                Log.e("Login Request", getRequestString("login.php", valuePairs))

                val resp = core.appUtility.callApiRequest(serverUrl + "login.php", valuePairs)
                Log.e("Login Response", resp)

                val jobj = JSONObject(resp)
                if (!jobj.getBoolean("status")) {
                    runOnUiThread {
                        rootVm.authState.value = Keys.LOGGED_OUT
                        AlertDialog.Builder(this@AuthActivity).setMessage("Wrong Username or Password.").setTitle("Login Failed").setPositiveButton("Close", null).create().show()
                    }
                } else {
                    if (jobj.getString("version") == "1.1.1") {

                        val user = DataTypeConverter().fromStringToUser(resp)
                        user.user_name = username
                        user.password = password
                        user.loggedIn = true

                        prefs.putString(Keys.USER_ID, user.user_id)
                        doAsync {
                            localDb?.userDao()?.insertUser(user)
                        }

                        runOnUiThread {
                            rootVm.authState.value = Keys.LOGGED_IN
                            rootVm.activeUser.value = user
                        }
                    } else {
                        runOnUiThread {
                            rootVm.authState.value = Keys.LOGGED_OUT
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                toast("Login Error" + ex.message)
                runOnUiThread {
                    rootVm.authState.value = Keys.LOGGED_OUT
                }
            }
        }
    }

    private fun completeProgress(bar: ProgressBar) {
        Thread(Runnable {
            try {
                TimeUnit.MILLISECONDS.sleep(500)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            for (i in 1..100) {
                try {
                    TimeUnit.MILLISECONDS.sleep(20)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                runOnUiThread {
                    bar.progress = i
                }
            }

            runOnUiThread {
                when (rootVm.authState.value) {
                    Keys.LOGGED_IN -> {
                        rootVm.authState.value = Keys.DOWNLOADING_DATA
                        rootVm.authStateMessage.value = "Downloading files and data..."
                        downloadStatus.setImageResource(R.drawable.ic_round_cloud_download_24px)
                        downloadLocationData()
                    }
                    Keys.DOWNLOADED_DATA -> {
                        rootVm.authStateMessage.value = "Loading map data on device..."
                        mapStatus.setImageResource(R.drawable.ic_round_map_24px)
                        val handle = Handler()
                        handle.postDelayed({
                            prefs.putBoolean(Keys.LOGGED, true)
                            startActivity(Intent(this@AuthActivity, HomeActivity::class.java))
                            finish()
                        }, 1000)
                    }
                }
            }
        }).start()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
