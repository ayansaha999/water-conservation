package org.horizen.waterconservation.Helper

object Keys {

    // AUTH STATUS
    const val LOGGING_IN = 0
    const val LOGGED_IN = 1
    const val DOWNLOADING_DATA = 2
    const val DOWNLOADED_DATA = 3
    const val LOADING_MAP = 4
    const val LOGGED_OUT = 5

    // PREFS KEY
    const val USER_ID = "USER_ID"
    const val LOGGED = "LOGGED"
    const val MAP_DATA_SYNCED = "MAP_DATA_SYNCED"

    // DATA KEY
    const val TITLE = "TITLE"
    const val PROGRESS = "PROGRESS"
    const val ACTION = "ACTION"

    // MAP LAYER KEY
    const val SCHEME_BOUNDARY = "SCHEME BOUNDARY"
    const val ZONE_BOUNDARY = "ZONE BOUNDARY"
    const val BLOCK_BOUNDARY = "BLOCK BOUNDARY"
    const val PANCHAYAT_BOUNDARY = "PANCHAYAT BOUNDARY"
    const val MOUZA_BOUNDARY = "MOUZA BOUNDARY"
    const val RAW_WATER_MAIN = "RAW WATER MAIN"
    const val CLEAR_WATER_MAIN = "CLEAR WATER MAIN"
    const val DISTRIBUTION_MAIN = "DISTRIBUTION MAIN"
    const val STAND_POST = "STAND POST"
    const val VALVE = "VALVE"
    const val OHR = "OVER HEAD RESERVOIR"

    // REQUEST KEY
    const val REQUEST_CHECK_SETTINGS = 1001
    const val REQUEST_CHECK_PERMISSION = 1002

}