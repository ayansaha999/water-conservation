package org.horizen.waterconservation.Helper

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import org.horizen.waterconservation.Daos.*
import org.horizen.waterconservation.Models.*

@Database(entities = [(User::class), (LocationData::class), (ProblemData::class), (ContactData::class),
    (ProblemLocation::class), (CustomMapLayer::class)], version = 2)
@TypeConverters(DataTypeConverter::class)
abstract class LocalDatabase: RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun locationDataDao(): LocationDao

    abstract fun problemDataDao(): ProblemDao

    abstract fun contactDataDao(): ContactDao

    abstract fun problemLocationDao(): ProblemLocationDao

    abstract fun customMapLayerDao(): CustomMapLayerDao

}