package org.horizen.waterconservation.Helper

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_progress.*
import org.horizen.waterconservation.R

class ProgressDialog : DialogFragment() {

    companion object {
        @JvmStatic
        fun newInstance(title: String, progress: Int, action: Int) = ProgressDialog().apply {
            val bundle = Bundle()
            bundle.putString(Keys.TITLE, title)
            bundle.putInt(Keys.PROGRESS, progress)
            bundle.putInt(Keys.ACTION, action)
            arguments = bundle
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val title = arguments?.getString(Keys.TITLE) as String
        val progress = arguments?.getInt(Keys.PROGRESS) as Int
        val action = arguments?.getInt(Keys.ACTION) as Int

        if (action == 1) {
            dialog.dismiss()
        } else {
            progressTitle.text = title
            progressBar.max = 100
            progressBar.progress = progress
            progressPercent.text = "$progress%"
        }

    }
}