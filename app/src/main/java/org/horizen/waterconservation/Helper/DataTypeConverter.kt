package org.horizen.waterconservation.Helper

import `in`.net.freak.androidcore.Map.GeoJson.MapLayerFeature
import `in`.net.freak.androidcore.ValuePair
import android.arch.persistence.room.TypeConverter
import com.google.android.gms.maps.model.LatLng

import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import org.horizen.waterconservation.Models.ContactData
import org.horizen.waterconservation.Models.LocationData
import org.horizen.waterconservation.Models.ProblemData
import org.horizen.waterconservation.Models.User
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class DataTypeConverter {

    @TypeConverter
    fun fromArrayMapFeatureToString(data: ArrayList<MapLayerFeature>): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToArrayMapFeature(data: String): ArrayList<MapLayerFeature> {
        val dataType = object : TypeToken<ArrayList<MapLayerFeature>>() {}.type
        return Gson().fromJson<ArrayList<MapLayerFeature>>(data, dataType)
    }

    @TypeConverter
    fun fromArrayValuePairToString(data: ArrayList<ValuePair>): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToArrayValuePair(data: String): ArrayList<ValuePair> {
        val dataType = object : TypeToken<ArrayList<ValuePair>>() {}.type
        return Gson().fromJson<ArrayList<ValuePair>>(data, dataType)
    }

    @TypeConverter
    fun fromUserToString(data: User): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToUser(data: String): User {
        val dataType = object : TypeToken<User>() {}.type
        return Gson().fromJson<User>(data, dataType)
    }

    @TypeConverter
    fun fromProblemDataToString(data: ProblemData): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToProblemData(data: String): ProblemData {
        val dataType = object : TypeToken<ProblemData>() {}.type
        return Gson().fromJson<ProblemData>(data, dataType)
    }

    @TypeConverter
    fun fromContactDataToString(data: ContactData): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToContactData(data: String): ContactData {
        val dataType = object : TypeToken<ContactData>() {}.type
        return Gson().fromJson<ContactData>(data, dataType)
    }

    @TypeConverter
    fun fromLocationToString(data: LocationData): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToLocation(data: String): LocationData {
        val dataType = object : TypeToken<LocationData>() {}.type
        return Gson().fromJson<LocationData>(data, dataType)
    }

    @TypeConverter
    fun fromLatLngToString(data: LatLng): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToLatLng(data: String): LatLng {
        val dataType = object : TypeToken<LatLng>() {}.type
        return Gson().fromJson<LatLng>(data, dataType)
    }

    fun fromObjectToJsonString(data: Any): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    fun fromObjectToArrayList(data: Any): ArrayList<ValuePair> {
        val dataString = fromObjectToJsonString(data)
        return fromStringToArrayList(dataString)
    }

    private fun fromStringToArrayList(dataString: String): ArrayList<ValuePair> {
        val result : ArrayList<ValuePair> = ArrayList()
        val json = JSONObject(dataString)
        val keys = json.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            try {
                val value = json.getString(key)
                result.add(ValuePair(key, value))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
        return result
    }
}