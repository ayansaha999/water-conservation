package org.horizen.waterconservation.Models

import `in`.net.freak.androidcore.Map.GeoJson.MapLayerFeature
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class CustomMapLayer(

        @NonNull
        @PrimaryKey
        var key: String,

        var feature: ArrayList<MapLayerFeature>,

        var visible: Boolean

): Serializable {
    constructor(): this("", ArrayList<MapLayerFeature>(), false)
}