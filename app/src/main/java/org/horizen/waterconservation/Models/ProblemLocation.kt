package org.horizen.waterconservation.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class ProblemLocation(

        @NonNull
        @PrimaryKey
        var id: String,
        var user_id: String,
        var pipiline_type: String,
        var gp_id: String,
        var zone_no: String,
        var mouza_name: String,
        var problem_id: String,
        var problem_lat: String,
        var problem_lon: String,

        var filePath: String,

        var resolved: Boolean,
        var remarks: String,

        var uploaded: Boolean,

        var date: String,
        var time: String
) : Serializable {
    constructor() : this("", "", "", "", "", "", "",
            "", "", "",  false, "", false, "", "")
}