package org.horizen.waterconservation.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

@Entity
data class User(

        @NonNull
        @PrimaryKey
        var user_id: String,
        var user_name: String,
        var password: String,
        var file_path: String,
        var version: String,

        var loggedIn: Boolean,

        var location: LatLng

): Serializable {
    constructor(): this("", "","", "", "", false, LatLng(0.0, 0.0))
}