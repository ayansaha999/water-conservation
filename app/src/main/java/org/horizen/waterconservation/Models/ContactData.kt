package org.horizen.waterconservation.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable


@Entity
data class ContactData(

        @NonNull
        @PrimaryKey
        var id: String,

        var receiver_name: String,
        var receiver_designation: String,
        var receiver_phno: String

): Serializable {
    constructor(): this("", "", "", "")
}