package org.horizen.waterconservation.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class LocationData(

        @PrimaryKey
        var id: String,
        var user_id: String,
        var district_id: String,
        var district_name: String,
        var block_id: String,
        var block_name: String,
        var gp_id: String,
        var gp_name: String,
        var project_id: String,
        var project_name: String
): Serializable {
    constructor(): this("", "", "", "", "", "", "", "", "", "")
}