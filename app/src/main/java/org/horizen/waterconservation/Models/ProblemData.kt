package org.horizen.waterconservation.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable


@Entity
data class ProblemData(

        @NonNull
        @PrimaryKey
        var id: String,

        var problem: String,

        var prioroty: String
): Serializable {
    constructor(): this("", "", "")
}