package org.horizen.waterconservation

import android.app.Application
import android.arch.persistence.room.Room
import android.support.multidex.MultiDex
import org.horizen.waterconservation.Helper.ConnectivityReceiver
import org.horizen.waterconservation.Helper.LocalDatabase

class RootApplication: Application() {

    companion object {
        var instance : RootApplication? = null
        var database: LocalDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        RootApplication.instance = this
        RootApplication.database = Room.databaseBuilder(this, LocalDatabase::class.java, "municipality-survey-db-v1").fallbackToDestructiveMigration().build()
    }

    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener) {
        ConnectivityReceiver.connectivityReceiverListener = listener
    }
}