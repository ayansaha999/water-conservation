package org.horizen.waterconservation.Home

import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_home.*
import org.horizen.waterconservation.Auth.AuthActivity
import org.horizen.waterconservation.Helper.Keys
import org.horizen.waterconservation.R
import org.horizen.waterconservation.Root.RootActivity
import org.jetbrains.anko.doAsync

class HomeActivity : RootActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_home)

        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { settingsResponse ->

        }

        task.addOnFailureListener { exception ->
            Log.e("Location Exception", exception.toString())
            if (exception is ResolvableApiException){
                try {
                } catch (sendEx: IntentSender.SendIntentException) {
                    Log.e("Location Exception", sendEx.toString())
                }
            }
        }

        rootVm.users?.observe(this, Observer { users ->
            if (users != null) {
                for (user in users) {
                    rootVm.activeUser.value = user
                }
            }
        })

        logout.setOnClickListener {
            prefs.putBoolean(Keys.LOGGED, false)
            startActivity(Intent(this@HomeActivity, AuthActivity::class.java))
            finish()
        }

        refresh.setOnClickListener {

        }

        report.setOnClickListener {
            startActivity(Intent(this@HomeActivity, MapActivity::class.java))
        }

        map.setOnClickListener {
            startActivity(Intent(this@HomeActivity, MapActivity::class.java))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
