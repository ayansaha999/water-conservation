package org.horizen.waterconservation.Home

import `in`.net.freak.androidcore.AndroidCore
import `in`.net.freak.androidcore.Picker.api.CameraImagePicker
import `in`.net.freak.androidcore.Picker.api.Picker
import `in`.net.freak.androidcore.Picker.api.callbacks.ImagePickerCallback
import `in`.net.freak.androidcore.Picker.api.entity.ChosenImage
import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.content_map.*
import kotlinx.android.synthetic.main.problem_bottom_sheet.*
import org.horizen.waterconservation.Helper.GlideApp
import org.horizen.waterconservation.Helper.Keys
import org.horizen.waterconservation.Helper.Util
import org.horizen.waterconservation.Models.ContactData
import org.horizen.waterconservation.Models.CustomMapLayer
import org.horizen.waterconservation.Models.ProblemData
import org.horizen.waterconservation.Models.ProblemLocation
import org.horizen.waterconservation.R

import org.horizen.waterconservation.Root.RootActivity
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class MapActivity : RootActivity(), OnMapReadyCallback, ImagePickerCallback {

    private var sheetBehavior: BottomSheetBehavior<*>? = null
    private var pipeTypeList = ArrayList<String>()
    private var problemList = ArrayList<ProblemData>()
    private var contactList = ArrayList<ContactData>()

    private var selectedContactList = ArrayList<ContactData>()

    private var problemIdList = ArrayList<String>()
    private var problemNameList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_map)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        rootVm.users?.observe(this, Observer { users ->
            if (users != null) {
                for (user in users) {
                    rootVm.activeUser.value = user
                }
            }
        })

        sheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        sheetBehavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {

            }

            override fun onStateChanged(view: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        sheetIndicator.setImageResource(R.drawable.ic_round_arrow_upward_24px)
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        sheetIndicator.setImageResource(R.drawable.ic_round_arrow_downward_24px)
                    }
                }
            }
        })

        zoomIn.setOnClickListener {
            zoomIn()
        }

        zoomOut.setOnClickListener {
            zoomOut()
        }

        myLocation.setOnClickListener {
            getLastLocation()
        }

        sheetAction.setOnClickListener {
            toggleProblemSheet()
        }

        pipeTypeList.add("Primary Rising Main")
        pipeTypeList.add("Secondary Rising Main")
        pipeTypeList.add("Distribution Main")

        val pipeTypeAd = ArrayAdapter<String>(this@MapActivity, R.layout.spinner_main_item, pipeTypeList)
        pipeTypeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_pipe.adapter = pipeTypeAd
        select_pipe.setSelection(0)

        rootVm.contacts?.observe(this, Observer { contacts ->
            contactList.clear()
            if (contacts != null) {
                contactList.addAll(contacts)
            }
            e("Contacts Size", "${contactList.size}")

            val contactsAd = ContactListAdapter(contactList)
            contactListView.adapter = contactsAd
        })

        rootVm.problems?.observe(this, Observer { problems ->
            problemList.clear()
            if (problems != null) {
                for (problem in problems) {
                    problemList.add(problem)
                    problemIdList.add(problem.id)
                    problemNameList.add(problem.problem)
                }
            }

            val problemTypeAd = ArrayAdapter<String>(this@MapActivity, R.layout.spinner_main_item, problemNameList)
            problemTypeAd.setDropDownViewResource(R.layout.spinner_list_item)
            select_problem.adapter = problemTypeAd
            select_problem.setSelection(0)

        })

        select_pipe.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val pl = rootVm.currentProblemLocation.value
                pl?.pipiline_type = pipeTypeList[position]
                rootVm.currentProblemLocation.value = pl
            }
        }

        select_problem.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val pl = rootVm.currentProblemLocation.value
                pl?.problem_id = problemIdList[position]
                rootVm.currentProblemLocation.value = pl
            }
        }

        capturePhoto.setOnClickListener {
            capture()
        }

        save.setOnClickListener {
            shareDetailsLay.visibility = View.VISIBLE
            done.visibility = View.VISIBLE
            save.visibility = View.GONE

            val pl = rootVm.currentProblemLocation.value
            pl?.id = UUID.randomUUID().toString()
            pl?.user_id = rootVm.activeUser.value?.user_id!!
            pl?.uploaded = false

            doAsync {
                localDb?.problemLocationDao()?.insertProblemLocation(pl!!)
            }
        }

        done.setOnClickListener {
            initializeForm()
        }
    }

    private fun toggleProblemSheet() {
        if (sheetBehavior?.state != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
        } else {
            sheetBehavior?.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap?) {
        mMap = map
        mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
        mMap!!.uiSettings.isMyLocationButtonEnabled = false

        if (core.appUtility.checkPermissions(this@MapActivity, permissions, 1009)) {
            mMap!!.isMyLocationEnabled = true
        }

        mMap!!.setOnMapClickListener { latLng ->

        }

        getCustomLayersFromRaw()
    }

    private fun getCustomLayersFromRaw() {
        runOnUiThread {
            val fileLayersArray = arrayListOf(R.raw.zone_boundary, R.raw.mouza_boundary, R.raw.gp_boundary,
                    R.raw.block_boundary, R.raw.scheme_boundary, R.raw.distribution_main, R.raw.clear_water_main, R.raw.raw_water_main)

            val fileKeysArray = arrayListOf(Keys.ZONE_BOUNDARY, Keys.MOUZA_BOUNDARY, Keys.PANCHAYAT_BOUNDARY,
                    Keys.BLOCK_BOUNDARY, Keys.SCHEME_BOUNDARY, Keys.DISTRIBUTION_MAIN, Keys.CLEAR_WATER_MAIN, Keys.RAW_WATER_MAIN)

            val layerColors = core.geoJsonColor
            val fileColorsArray = arrayListOf(layerColors.zone_boundary, layerColors.mouza_boundary, layerColors.gp_boundary,
                    layerColors.block_boundary, layerColors.scheme_boundary, layerColors.distribution_main_undamaged, layerColors.secondary_rising_main,
                    layerColors.primary_rising_main)

            for (index in 0 until fileLayersArray.size) {
                val customLayer = CustomMapLayer()

                val file = fileLayersArray[index]
                val layerFile = AndroidCore.appUtility.getStringFromRawFile(applicationContext, file)
                val layerFeatures = AndroidCore.geoJsonFeature.getFeatures(layerFile, mMap!!, null)
                for (feature in layerFeatures) {
                    feature.color = fileColorsArray[index]
                    feature.width = 5f
                }

                customLayer.key = fileKeysArray[index]
                customLayer.feature = layerFeatures
                customLayer.visible = true

                doAsync {
                    localDb?.customMapLayerDao()?.insertCustomMapLayer(customLayer)

                    if (index == (fileLayersArray.size - 1)) {
                        runOnUiThread {
                            loadLayers()
                        }
                    }
                }
            }

        }
    }

    private fun loadLayers() {
        mMap!!.clear()
        doAsync {
            val custLayers = localDb?.customMapLayerDao()?.getAllCustomMapLayers()
            if (custLayers != null) {
                for (layer in custLayers) {
                    if (layer.visible) {
                        val features = layer.feature
                        core.geoJsonFeature.loadOnMap(features, mMap!!)
                    }
                }
            }
        }
    }

    private fun zoomIn() {
        mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
    }

    private fun zoomOut() {
        mMap!!.animateCamera(CameraUpdateFactory.zoomOut())
    }

    inner class ContactListAdapter(private val contacts: ArrayList<ContactData>): BaseAdapter() {
        private val mInflater: LayoutInflater = LayoutInflater.from(this@MapActivity)

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = mInflater.inflate(R.layout.single_contact, parent, false)

            val contact = contacts[position]
            val name = view.findViewById<TextView>(R.id.name)
            val des = view.findViewById<TextView>(R.id.designation)
            val check = view.findViewById<CheckBox>(R.id.checkbox)
            val root = view.findViewById<LinearLayout>(R.id.itemRoot)

            name.text = contact.receiver_name
            des.text = contact.receiver_designation

            check.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    if (!selectedContactList.contains(contact)) {
                        selectedContactList.add(contact)
                    }
                } else {
                    if (selectedContactList.contains(contact)) {
                        selectedContactList.remove(contact)
                    }
                }
            }

            root.setOnClickListener {
                check.isChecked = !check.isChecked
            }

            return view
        }

        override fun getItem(position: Int): Any {
            return contacts[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return contacts.size
        }
    }

    private fun initializeForm() {
        toggleProblemSheet()
        save.visibility = View.VISIBLE
        done.visibility = View.GONE
        shareDetailsLay.visibility = View.GONE
        select_pipe.setSelection(0)
        selectedContactList.clear()
        val contactsAd = ContactListAdapter(contactList)
        contactListView.adapter = contactsAd
    }

    override fun onImagesChosen(chosenImages: MutableList<ChosenImage>?) {
        for (chosenImage in chosenImages!!) {
            val p = chosenImage.originalPath
            val currPath = getOutputMediaFileUri()
            Util.resizeImage(p, currPath.absolutePath)
            capturedImage.visibility = View.VISIBLE
            GlideApp.with(this@MapActivity).load(currPath.absolutePath)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(capturedImage)
        }
    }

    override fun onError(error: String?) {
        e("Error", error)
    }

    private var cameraPicker: CameraImagePicker? = null
    private var pickerPath: String? = null

    private fun capture() {
        cameraPicker = CameraImagePicker(this)
        cameraPicker!!.setDebugglable(true)
        cameraPicker!!.setImagePickerCallback(this)
        pickerPath = cameraPicker!!.pickImage()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = CameraImagePicker(this)
                    cameraPicker!!.setImagePickerCallback(this)
                    cameraPicker!!.reinitialize(pickerPath)
                }
                cameraPicker!!.submit(data)
            }
        }
    }

    private fun getOutputMediaFileUri(): File {
        var uploadDir = File(Environment.getExternalStorageDirectory().absolutePath + "/CPP")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        uploadDir = File(uploadDir, "images")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        return File(uploadDir, System.currentTimeMillis().toString() + ".jpg")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
