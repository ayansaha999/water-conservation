package org.horizen.waterconservation.Root

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import org.horizen.waterconservation.Models.*
import org.horizen.waterconservation.RootApplication

class RootVm: ViewModel() {

    var authState = MutableLiveData<Int>()
    var activeUser = MutableLiveData<User>()
    var authStateMessage = MutableLiveData<String>()

    var internetAvailable = MutableLiveData<Boolean>()

    var currentLocaton = MutableLiveData<Location>()

    var users : LiveData<List<User>>? = null
    var problems : LiveData<List<ProblemData>>? = null
    var contacts : LiveData<List<ContactData>>? = null

    var currentProblemLocation = MutableLiveData<ProblemLocation>()

    init {
        currentProblemLocation.value = ProblemLocation()
        users = RootApplication.database?.userDao()?.getAllUsers()
        problems = RootApplication.database?.problemDataDao()?.getAllProblemDatas()
        contacts = RootApplication.database?.contactDataDao()?.getAllContactDatas()
    }

    fun getUserLocation(id: String): LiveData<LatLng>? {
        return RootApplication.database?.userDao()?.findUserLocation(id)
    }
}