package org.horizen.waterconservation.Root

import `in`.net.freak.androidcore.AndroidCore
import `in`.net.freak.androidcore.Pref
import `in`.net.freak.androidcore.ValuePair
import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Log.e
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import org.horizen.waterconservation.Helper.ConnectivityReceiver
import org.horizen.waterconservation.Helper.DataTypeConverter
import org.horizen.waterconservation.Helper.Keys
import org.horizen.waterconservation.Helper.LocalDatabase
import org.horizen.waterconservation.Models.CustomMapLayer
import org.horizen.waterconservation.Models.User
import org.horizen.waterconservation.R
import org.horizen.waterconservation.RootApplication
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import java.util.*

@SuppressLint("Registered")
open class RootActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {
    protected val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO)

    protected lateinit var rootVm: RootVm
    protected val serverUrl = "http://maps.wbphed.gov.in/water_conservation/webservice/"
    protected val serverFileUrl = "${serverUrl}data/kml/"

    protected lateinit var core: AndroidCore
    protected lateinit var prefs: Pref
    protected var localDb: LocalDatabase? = null
    protected var user: User? = null

    protected var mMap: GoogleMap? = null

    protected var builder = LocationSettingsRequest.Builder()
    protected lateinit var fusedLocationClient: FusedLocationProviderClient
    protected lateinit var locationCallback: LocationCallback

    protected val locationRequest = LocationRequest().apply {
        interval = 5000
        fastestInterval = 2000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    protected var requestingLocationUpdates = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        core = AndroidCore
        prefs = Pref(applicationContext)
        localDb = RootApplication.database
        rootVm = ViewModelProviders.of(this).get(RootVm::class.java)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { settingsResponse ->
            e("LocationService", "Settings Request granted!")
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(this@RootActivity, Keys.REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    e("Location Update Worker", "${location.latitude}, ${location.longitude}")
                    val lastLoc = rootVm.currentLocaton.value
                    if (lastLoc != null) {
                        if (lastLoc != location) {
                            rootVm.currentLocaton.value = location
                        }
                    } else {
                        rootVm.currentLocaton.value = location
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        RootApplication.instance!!.setConnectivityListener(this)
        if (requestingLocationUpdates) startLocationUpdates()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        rootVm.internetAvailable.value = isConnected
    }

    fun downloadLocationData() {
        rootVm.activeUser.observe(this, Observer { userVal ->
            if (userVal != null) {
                user = userVal
            }
        })

        rootVm.authStateMessage.value = "Downloading location data..."

        doAsync {
            try {
                val valuePairs = ArrayList<ValuePair>()
                valuePairs.add(ValuePair("userid", user!!.user_id))

                Log.e("Login Request", getRequestString("get_assigned_gp.php", valuePairs))
                val resp = core.appUtility.callApiRequest(serverUrl + "get_assigned_gp.php", valuePairs)
                Log.e("Login Response", resp)

                val jobj = JSONArray(resp)
                for (index in 0 until jobj.length()) {
                    val obj = jobj.getJSONObject(index)
                    val location = DataTypeConverter().fromStringToLocation(obj.toString())
                    localDb?.locationDataDao()?.insertLocationData(location)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            downloadProblemData()
        }
    }

    private fun downloadProblemData() {
        runOnUiThread {
            rootVm.authStateMessage.value = "Downloading problem related data..."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            Log.e("Problem Request", getRequestString("get_problem.php", valuePairs))
            val resp = core.appUtility.callApiRequest(serverUrl + "get_problem.php", valuePairs)
            Log.e("Problem Response", resp)

            val jobj = JSONArray(resp)
            for (index in 0 until jobj.length()) {
                val obj = jobj.getJSONObject(index)
                val location = DataTypeConverter().fromStringToProblemData(obj.toString())
                localDb?.problemDataDao()?.insertProblemData(location)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        downloadContactsData()
    }

    private fun downloadContactsData() {
        runOnUiThread {
            rootVm.authStateMessage.value = "Downloading contacts related data..."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("userid", user!!.user_id))
            Log.e("Contact Request", getRequestString("get_userlist.php", valuePairs))
            val resp = core.appUtility.callApiRequest(serverUrl + "get_userlist.php", valuePairs)
            Log.e("Contact Response", resp)

            val jobj = JSONArray(resp)
            for (index in 0 until jobj.length()) {
                val obj = jobj.getJSONObject(index)
                val location = DataTypeConverter().fromStringToContactData(obj.toString())
                localDb?.contactDataDao()?.insertContactData(location)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        runOnUiThread {
            rootVm.authState.value = Keys.DOWNLOADED_DATA

        }
    }

    fun getRequestString(api: String, valuePairs: ArrayList<ValuePair>): String {
        var request = "$serverUrl$api?"
        for (valuePair in valuePairs) {
            request += "${valuePair.name}=${valuePair.value}&&"
        }
        return request
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            if (location != null) {
                if (mMap != null) {
                    val cameraUpdate = CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(LatLng(location.latitude, location.longitude), 20f))
                    mMap!!.animateCamera(cameraUpdate)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Keys.REQUEST_CHECK_SETTINGS) {
            e("LocationService", "Settings Granted on Request!")
        }
    }

    private fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null /* Looper */)
        requestingLocationUpdates = false
    }
}