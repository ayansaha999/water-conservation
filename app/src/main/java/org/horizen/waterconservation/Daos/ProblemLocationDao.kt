package org.horizen.waterconservation.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.waterconservation.Models.ProblemLocation

@Dao
interface ProblemLocationDao {

    @Query("select * from ProblemLocation")
    fun getAllProblemLocations(): LiveData<List<ProblemLocation>>

    @Query("select * from ProblemLocation where id = :id")
    fun findProblemLocationById(id: String): ProblemLocation

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProblemLocation(vararg ProblemLocation: ProblemLocation)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateProblemLocation(vararg ProblemLocation: ProblemLocation)

    @Delete
    fun deleteProblemLocation(ProblemLocation: ProblemLocation)
}