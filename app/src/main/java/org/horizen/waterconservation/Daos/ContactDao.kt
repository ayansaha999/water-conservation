package org.horizen.waterconservation.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.waterconservation.Models.ContactData

@Dao
interface ContactDao {

    @Query("select * from ContactData order by receiver_name")
    fun getAllContactDatas(): LiveData<List<ContactData>>

    @Query("select * from ContactData where id = :id")
    fun findContactDataById(id: String): ContactData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContactData(vararg ContactData: ContactData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateContactData(vararg ContactData: ContactData)

    @Delete
    fun deleteContactData(ContactData: ContactData)
}