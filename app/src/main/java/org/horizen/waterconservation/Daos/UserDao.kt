package org.horizen.waterconservation.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.google.android.gms.maps.model.LatLng
import org.horizen.waterconservation.Models.User

@Dao
interface UserDao {

    @Query("select * from User order by user_name")
    fun getAllUsers(): LiveData<List<User>>

    @Query("select * from User where user_id = :id")
    fun findUserById(id: String): User

    @Query("select location from User where user_id = :id")
    fun findUserLocation(id: String): LiveData<LatLng>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(vararg User: User)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUser(vararg User: User)

    @Delete
    fun deleteUser(User: User)
}