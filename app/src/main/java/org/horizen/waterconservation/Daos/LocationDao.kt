package org.horizen.waterconservation.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.waterconservation.Models.LocationData

@Dao
interface LocationDao {

    @Query("select * from LocationData order by project_name")
    fun getAllLocationDatas(): LiveData<List<LocationData>>

    @Query("select * from LocationData where project_id = :id")
    fun findLocationDataById(id: String): LocationData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocationData(vararg LocationData: LocationData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateLocationData(vararg LocationData: LocationData)

    @Delete
    fun deleteLocationData(LocationData: LocationData)
}