package org.horizen.waterconservation.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.waterconservation.Models.ProblemData

@Dao
interface ProblemDao {

    @Query("select * from ProblemData order by problem")
    fun getAllProblemDatas(): LiveData<List<ProblemData>>

    @Query("select * from ProblemData where id = :id")
    fun findProblemDataById(id: String): ProblemData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProblemData(vararg ProblemData: ProblemData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateProblemData(vararg ProblemData: ProblemData)

    @Delete
    fun deleteProblemData(ProblemData: ProblemData)
}