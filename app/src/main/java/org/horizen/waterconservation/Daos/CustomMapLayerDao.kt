package org.horizen.waterconservation.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.waterconservation.Models.CustomMapLayer

@Dao
interface CustomMapLayerDao {

    @Query("select * from CustomMapLayer")
    fun getAllCustomMapLayers(): List<CustomMapLayer>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCustomMapLayer(vararg CustomMapLayer: CustomMapLayer)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateCustomMapLayer(vararg CustomMapLayer: CustomMapLayer)

    @Delete
    fun deleteCustomMapLayer(CustomMapLayer: CustomMapLayer)
}